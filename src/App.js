import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import ShowVC from './pages/ShowVC';
import Connect from './pages/Connect';
import 'bootstrap/dist/css/bootstrap.min.css';
import LoginPage from "./pages/LoginPage";
import SetWallet from "./pages/SetWallet";
import ManageVc from "./pages/ManageVc";
import ManageGroup from "./pages/ManageGroup";

function App() {
    return (
        <Router>
                <Routes>
                    <Route exact path="/home" element={<Home />} />
                    <Route exact path="/" element={<Connect />} />
                    <Route exact path="/allvc" element={<ShowVC />} />
                    <Route path="/login" element={<LoginPage />} />
                    <Route path="/addwallet" element={<SetWallet />} />
                    <Route path="/manage_vc" element={<ManageVc />} />
                    <Route path="/manage_group" element={<ManageGroup />} />
                </Routes>
            </Router>
    );
}

export default App;
