import React from 'react';
import {
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions,
    Button,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Checkbox,
    ListItemText,
    OutlinedInput
} from '@mui/material';

const DialogAuthorizedComponent = ({
                                       open,
                                       handleClose,
                                       employeesData,
                                       personName,
                                       listOfGroup,
                                       groupSelection,
                                       handleChange,
                                       handleGroupChange,
                                       handleAddVC
                                   }) => (
    <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Add VC</DialogTitle>
        <DialogContent>
            <DialogContentText>Please choose employee and group.</DialogContentText>
            <FormControl sx={{m: 1, width: 500}}>
                <InputLabel id="demo-multiple-checkbox-label">Email</InputLabel>
                <Select
                    labelId="demo-multiple-checkbox-label"
                    id="demo-multiple-checkbox"
                    multiple
                    value={personName}
                    onChange={handleChange}
                    input={<OutlinedInput label="Tag"/>}
                    renderValue={(selected) => selected.join(', ')}
                >
                    {employeesData.map((name) => (
                        <MenuItem key={name.email} value={name.email}>
                            <Checkbox checked={personName.indexOf(name.email) > -1}/>
                            <ListItemText primary={name.firstName + " " + name.lastName + " (" + name.email + ")"}/>
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>

            <FormControl sx={{ m: 1, width: '100%' }}>
                <InputLabel id="group-select-label">Group</InputLabel>
                <Select
                    labelId="group-select-label"
                    id="demo-multiple-checkbox-group"
                    multiple
                    value={groupSelection}
                    onChange={handleGroupChange}
                    input={<OutlinedInput label="Group" />}
                    renderValue={(selected) => selected.join(', ')}
                >
                    {listOfGroup.map((group) => (
                        <MenuItem key={group} value={group}>
                            <Checkbox checked={groupSelection.indexOf(group) > -1} />
                            <ListItemText primary={group} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </DialogContent>
        <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button onClick={handleAddVC}>ADD</Button>
        </DialogActions>
    </Dialog>
);

export default DialogAuthorizedComponent;
