import React from 'react';
import styles from '../css/home.module.css';
const Header = () => {
    return (
        <header class='row m-0'>
            <div className={styles.header}>
                <div>
                    <img className="logo" src="/GaiaxBlueLogo.svg" alt="Logo" />
                </div>
                <div className={styles.buttons}>
                    <div
                        className={styles.highlight}>
                        <w3m-network-button />
                    </div>
                    <div className={styles.highlight} >
                        <w3m-button balance={"hide"} />
                    </div>
                </div>
            </div>
        </header>
    );
};
export default Header;
