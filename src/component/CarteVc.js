import React from 'react';
import {Button} from "@mui/material";
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
const CarteVC = ({ vc,handleShowVC ,handleAssignVC}) => {
    return (
                <div className="col-lg-3 col-md-4 col-sm-6 col-12   flex-grow-0" style={{ marginTop: '1em', display: "flex", justifyContent: "center" }}>
                    <div className="card h-100 shadow-sm cardHoverEffect">
                        <div className="card-body cardBodyFlex">
                            <p className=''>{vc.isShare ? <CheckCircleIcon fontSize="large" color="success" />: <CheckCircleIcon fontSize="large" sx={{ color: 'red' }} />}</p>
                            <div className="card-text cardContent">
                                <p className='label'>Type</p>
                                <p className='title'>{vc.data? ( vc.data.parsedDocument.credentialSubject['type'] || vc.data.parsedDocument.credentialSubject['@type'] || "Type is not available") : "Credential Subject is not available"}</p>
                                <p className='label'>Issuer</p>
                                <p className='text'>{vc.data.parsedDocument.issuer || "Issuer is not available"}</p>
                                <p className='label'>Issuance Date</p>
                                <p className='text'>{vc.data.parsedDocument.issuanceDate  || "Issuance Date is not available"}</p>
                            </div>
                            <div className='btnBottom text-center'>
                            <Button className='m-2 text-center w-100' style={{backgroundColor:"#000F8E"}}  variant="contained" onClick={() => handleShowVC(vc.data.parsedDocument) }>Show Vc</Button>
                            <Button className='m-2 text-center w-100' style={{ backgroundColor: '#000F8E', color: 'white', pointerEvents: !vc.isShare ? 'none' : 'auto', opacity: !vc.isShare ? '0.5' : '1' }} variant="contained" onClick={() => handleAssignVC(vc.data.parsedDocument)}>Assigne Vc</Button>
                        </div>
                        </div>
                    </div>
                </div>
    );
};

export default CarteVC;
