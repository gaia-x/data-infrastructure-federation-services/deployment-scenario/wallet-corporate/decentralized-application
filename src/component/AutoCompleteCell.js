import React, { useState, useEffect } from 'react';
import '../css/Home.css';
import {
    Autocomplete,
    Backdrop,
    Button,
    Chip,
    CircularProgress,
    IconButton,
    TextField, Tooltip
} from '@mui/material';
import Web3 from 'web3';
import CredentialStorage from '../data/abi/CredentialStorage.json';
import { config } from '../config/config';
import { useAccount } from 'wagmi';
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogActions from "@mui/material/DialogActions";
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from "@mui/icons-material/Delete";
const AutocompleteCell = ({ id, listEmployees, nameGroup,handleLoad }) => {
    const ACCOUNT_ADDRESS = config.address;
    const LIMIT = 3;
    const { address } = useAccount();
    const [inputValue, setInputValue] = useState('');
    const [listEmployeesGroup, setListEmployeesGroup] = useState([]);
    const [listOfAllEmployees, setListOfAllEmployees] = useState([]);
    const [listOfAllAdress, setListOfAllAdress] = useState([]);
    const [listToAdd, setListToAdd] = useState([]);
    const [openDialog, setOpenDialog] = useState(false);
    const [isAdd, setIsAdd] = useState(false);
    const [isRemove, setIsRemove] = useState(false);
    const [isRemoveAll, setIsRemoveAll] = useState(false);
    const [message, setMessage] = useState("");
    const [openLoad, setOpenLoad] = useState(false);
    const [openAEditDialogGroup, setOpenAEditDialogGroup] = useState(false);

    useEffect(() => {
        handleInit(listEmployees, nameGroup);
    }, [listEmployees, nameGroup]);

    const handleInit = async (allEmployees, groupName) => {
        try {
            const listAdressOfEmployee = await handleEmployeeList(groupName);
            const listEmployeeOfGroup = getNameAndLastNameByAddress(allEmployees, listAdressOfEmployee);
            const allEmployeesOfCompany = extractNamesToTable(allEmployees);
            setListEmployeesGroup(listEmployeeOfGroup);
            setListOfAllEmployees(allEmployeesOfCompany);
        } catch (error) {
            console.error('Error initializing data:', error);
        }
    };

    const handleEmployeeList = async (name) => {
        try {
            const provider = new Web3(window.ethereum);
            const contract = new provider.eth.Contract(CredentialStorage.abi, ACCOUNT_ADDRESS);
            const membersOfGroup = await contract.methods.getMembersOfGroup(name).call({ from: address });
            return membersOfGroup;
        } catch (error) {
            console.error('Error getting members of group:', error);
            return [];
        }
    };

    const getNameAndLastNameByAddress = (employees, addressData) => {
        const result = [];
        addressData.forEach((entry) => {
            const user = employees.find((user) => user.walletData === entry);
            if (user) {
                const fullName = `${user.firstName} ${user.lastName}`;
                result.push(fullName);
            }
        });
        return result;
    };
    const getAddressByNameAndLastName = (employees, name) => {
        let adressWallet = null;
            const users = employees.find((user) => `${user.firstName} ${user.lastName}` === name);
            if (users) {
             adressWallet= users.walletData;
            }
        return adressWallet;
    };

    const extractNamesToTable = (users) => {
        return users.map(user => `${user.firstName} ${user.lastName}`);
    };

    const handleGroupChange = (event, newInputValue) => {
        setInputValue(newInputValue);
    };

    const handleAutocompleteChange = (event, newValue) => {
        setIsRemoveAll(false)
        setIsAdd(false)
        setIsRemove(false);

        const addresses = [];
        // Determine values added
        const addedValues = newValue.filter(value => !listEmployeesGroup.includes(value));
        if (addedValues.length > 0) {
            addedValues.forEach(employee => {
                const addressEmployee = getAddressByNameAndLastName(listEmployees, employee);
                addresses.push(addressEmployee);
            });
            setListOfAllAdress(addresses)
            setMessage('do you wish to add the employee of group ' +nameGroup)
            setIsAdd(true);
            setOpenDialog(true)
        }

        // Determine values removed
        const removedValues = listEmployeesGroup.filter(value => !newValue.includes(value));
        if (removedValues.length > 0) {
            removedValues.forEach(employee => {
                const addressEmployee = getAddressByNameAndLastName(listEmployees, employee);
                addresses.push(addressEmployee);
            });

            if(addresses.length==1){
                setMessage('do you wish to delete the employee of group ' +nameGroup+'.')
                setIsRemove(true)
            }else if(addresses.length>1){
                setMessage('do you want to delete all employees of group ' +nameGroup+'.')
                setIsRemoveAll(true)
        }
            setListOfAllAdress(addresses)
            setOpenDialog(true)
        }
        setListToAdd(newValue)
    };
    const handleCloseDialog = () => {
        setOpenDialog(false);
    };
    const handleCloseEditDialog = () => {
        setOpenAEditDialogGroup(false);
    };
    const handleEdit= () => {
        setOpenAEditDialogGroup(true);
    };
    const handleRemoveEmployeeGroup = async () => {
        if(listOfAllAdress.length >0){
            console.log(listOfAllAdress);
            await  handleRemoveEmployeeToGroup(listOfAllAdress)
        }

    };
    const handleAddEmployeeGroup = async () => {
        if(listOfAllAdress.length >0){
            console.log(listOfAllAdress);
           await  handleAddEmployeeToGroup(listOfAllAdress)
        }
    };

    const handleAddEmployeeToGroup = async (adressEmployees) => {
            try {
               handleLoad(true);
               setOpenAEditDialogGroup(false)
               handleCloseDialog()
                // Fonction pour gérer le partage de chaque VC et la mise à jour des tableaux
                if (typeof window.ethereum !== "undefined") {
                    const provider = new Web3(window.ethereum);
                    const contract = new provider.eth.Contract(CredentialStorage.abi, ACCOUNT_ADDRESS);
                    const resultAddEmployee= await contract.methods.addMembersToGroup(nameGroup,adressEmployees).send({from: address});
                    if(resultAddEmployee){
                        window.location.reload(true);
                    }
                }
            } catch (error) {
                handleLoad(false)
                setIsRemoveAll(false)
                setIsAdd(false)
                setIsRemove(false)
                setMessage("Error when adding an employee to a group.")
                setOpenDialog(true)

            }
        }
    const handleRemoveEmployeeToGroup = async (adressEmployees) => {
            try {
               handleLoad(true);
                setOpenAEditDialogGroup(false)
               handleCloseDialog()
                // Fonction pour gérer le partage de chaque VC et la mise à jour des tableaux
                if (typeof window.ethereum !== "undefined") {
                    const provider = new Web3(window.ethereum);
                    const contract = new provider.eth.Contract(CredentialStorage.abi, ACCOUNT_ADDRESS);
                    const resultAddEmployee= await contract.methods.removeMembersFromGroup(nameGroup,adressEmployees).send({from: address});
                    if(resultAddEmployee){
                        window.location.reload(true);
                    }
                }
            } catch (error) {
                handleLoad(false)
                setIsRemoveAll(false)
                setIsAdd(false)
                setIsRemove(false)
                setMessage("Error when remove  employee to a group.")
                setOpenDialog(true)

            }
        }

    return (
        <div style={{ display: 'flex', alignItems: 'center', gap: '10px', width: '100%' }}>
            <Backdrop
                sx = {{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open = {openLoad}>
                <CircularProgress color = "inherit" />
            </Backdrop>
            <Dialog
                open={openDialog}
                onClose={handleCloseDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Message
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDialog}>
                        Cancel
                    </Button>
                    {isAdd ? <Button onClick={handleAddEmployeeGroup}>Add employee </Button>: "" }
                    {isRemove ? <Button onClick={handleRemoveEmployeeGroup}>Remove employee</Button>: "" }
                    {isRemoveAll ? <Button onClick={handleRemoveEmployeeGroup}>Remove all employees</Button> : "" }
                </DialogActions>
            </Dialog>
            {!openAEditDialogGroup ? (
                <>
                    <Autocomplete
                        multiple
                        style={{border: "0px solid black" }}
                        id={`tags-filled-${id}`}
                        freeSolo
                        limitTags={3}
                        sx={{ width: '100%' }}
                        options={listOfAllEmployees}
                        filterSelectedOptions
                        value={listEmployeesGroup}
                        disabled={listEmployeesGroup.length > LIMIT}
                        onChange={handleAutocompleteChange}
                        inputValue={inputValue}
                        onInputChange={handleGroupChange}
                        renderTags={(value, getTagProps) =>{
                          return (  value.map((option, index) => (
                                <Chip key={index} variant="outlined" label={option} {...getTagProps({ index })}
                                      sx={{  backgroundColor: '#000F8E !important' ,
                                          color: 'white !important' }}
                                      deleteIcon={
                                          <Tooltip title="Delete Employee">
                                          <IconButton
                                              size="small"
                                              style={{ color: 'white' ,}}
                                          >
                                              <DeleteIcon />
                                          </IconButton>
                                          </Tooltip>
                                      }
                                />
                            ))
                          ) }
                        }
                        renderInput={(params) => {
                            const isOverLimit = listEmployeesGroup.length > LIMIT;

                            return isOverLimit ? (
                                <TextField
                                    {...params}
                                    InputLabelProps={{ shrink: false }}
                                    InputProps={{
                                        ...params.InputProps,
                                        classes: {
                                            underline: 'textFieldUnderline', // Nom de la classe pour la bordure inférieure
                                            focused: 'textFieldFocused', // Nom de la classe pour le focus
                                        },
                                    }}
                                    id="standard-basic" variant="standard"
                                />
                            ) : (
                                <TextField
                                    {...params}
                                    placeholder="Write employee name"
                                    InputLabelProps={{ shrink: true }}
                                    InputProps={{
                                    ...params.InputProps,
                                        classes: {
                                            underline: 'textFieldUnderline', // Nom de la classe pour la bordure inférieure
                                            focused: 'textFieldFocused', // Nom de la classe pour le focus
                                        },
                                    }}
                                    id="standard-basic" variant="standard"
                                />
                            );
                        }}


                    />

                    { listEmployeesGroup.length > LIMIT ? (<Tooltip title="Edit"> <IconButton
                        aria-label="delete"
                        size="small"
                        onClick={handleEdit}
                        style={{
                            backgroundColor: '#000F8E',
                            color: 'white',
                        }}
                    >
                        <EditIcon fontSize="inherit" />
                    </IconButton> </Tooltip>):""
                    }
                </>
            ) : (
                <Dialog
                    open={openAEditDialogGroup}
                    onClose={handleCloseEditDialog}
                    fullWidth={'xl'}
                    maxWidth={'xl'}>
                    <DialogTitle>Edit group {nameGroup}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please edit group.
                        </DialogContentText>
                        <div>
                            <Autocomplete
                                multiple
                                id={`tags-filled-${id}`}
                                sx={{ width: '100%' }}
                                options={listOfAllEmployees}
                                filterSelectedOptions
                                value={listEmployeesGroup}
                                onChange={handleAutocompleteChange}
                                inputValue={inputValue}
                                onInputChange={handleGroupChange}
                                renderTags={(value, getTagProps) =>
                                    value.map((option, index) => (
                                        <Chip key={index} variant="outlined" label={option} {...getTagProps({ index })}
                                              sx={{  backgroundColor: '#000F8E',
                                                  color: 'white' }}
                                              deleteIcon={
                                                  <Tooltip title="Delete Employee">
                                                  <IconButton
                                                      size="small"
                                                      style={{ color: 'white' }}
                                                  >
                                                      <DeleteIcon />
                                                  </IconButton>
                                                  </Tooltip>
                                              }
                                        />
                                    ))
                                }
                                renderInput={(params) => (
                                    <TextField {...params} placeholder="Write employee name" InputLabelProps={{ shrink: true }} />
                                )}
                            />
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleCloseEditDialog}>Cancel</Button>
                    </DialogActions>
                </Dialog>
            )}
        </div>
    );
};

export default AutocompleteCell;
