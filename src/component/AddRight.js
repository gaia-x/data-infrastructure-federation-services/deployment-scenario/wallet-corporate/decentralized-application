import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import {Button, Checkbox, FormControlLabel, FormGroup} from "@mui/material";
import DialogActions from "@mui/material/DialogActions";
import Dialog from "@mui/material/Dialog";
import * as React from "react";
import {useState} from "react";


const AddRight = ({
                      open,
                      handleClose,
                      handleChange,
                      addRight,
                      checkedValues
                  }) => (
    <Dialog
        open={open}
        onClose={handleClose}
        fullWidth={'sm'}
        maxWidth={'sm'}>
        <DialogTitle>Choosing a right to add</DialogTitle>
        <DialogContent>
            <div>
                <FormGroup>
                    <FormControlLabel
                        control={<Checkbox checked={checkedValues.Read} onChange={handleChange} name="Read"
                                           style={{color: '#000F8E'}}/>}
                        label="Read"
                    />
                    <FormControlLabel
                        control={<Checkbox checked={checkedValues.Consumption} onChange={handleChange}
                                           name="Consumption" style={{color: '#000F8E'}}/>}
                        label="Consumption"
                    />
                    <FormControlLabel
                        control={<Checkbox checked={checkedValues.Presentation} onChange={handleChange}
                                           name="Presentation" style={{color: '#000F8E'}}/>}
                        label="Presentation"
                    />
                    <FormControlLabel
                        control={<Checkbox checked={checkedValues.Import} onChange={handleChange} name="Import"
                                           style={{color: '#000F8E'}}/>}
                        label="Import"
                    />
                    <FormControlLabel
                        control={<Checkbox checked={checkedValues.WalletCorporate} onChange={handleChange}
                                           name="WalletCorporate" style={{color: '#000F8E'}}/>}
                        label="WalletCorporate"
                    />
                </FormGroup>
            </div>
        </DialogContent>
        <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button onClick={addRight}>Add rights</Button>
        </DialogActions>
    </Dialog>
);

export default AddRight;