export const getCookieValue=(cookieName) =>{
    const cookies = document.cookie.split('; ');
    for (const cookie of cookies) {
        const [name, value] = cookie.split('=');
        if (name === cookieName) {
            return value;
        }
    }
    return '';
}
export const deleteCookie = (cookieName) => {
    document.cookie = `${cookieName}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
};
// Fonction pour stocker un objet JSON dans le localStorage
export const  setLocalStorageItem=(key, value) =>{
    const jsonValue = JSON.stringify(value);
    localStorage.setItem(key, jsonValue);
}
// Fonction pour récupérer un objet JSON à partir du localStorage
export const getLocalStorageItem=(key) =>{
    // Récupérer la chaîne JSON du localStorage en utilisant la clé spécifiée
    const jsonValue = localStorage.getItem(key);

    // Si la valeur récupérée n'est pas null, la convertir en objet JSON et la renvoyer
    if (jsonValue !== null) {
        return JSON.parse(jsonValue);
    }

    // Sinon, renvoyer null
    return null;
}
export const removeLocalStorageItem=(key)  =>{
    localStorage.removeItem(key);
}

export const roundDecimalToNextInteger= (decimal) => {
    // Check if the decimal part is greater than zero
    if (decimal % 1 !== 0) {
        // If yes, use Math.ceil to round to the next integer
        return Math.ceil(decimal);
    } else {
        // Otherwise, return the integer as it is
        return decimal;
    }
}
export const  getFirstNElements=(array, count)=> {
    // Nombre d'éléments à récupérer par page
    const elementsPerPage = 8;

    // Calcule l'index de début et de fin en fonction du count
    const startIndex = (count - 1) * elementsPerPage;
    const endIndex = startIndex + elementsPerPage;

    // Utilisation de la méthode slice pour récupérer les éléments
    return array.slice(startIndex, endIndex);
}
export const getAllKeys=(object) =>{
    return Object.keys(object);
}
export const  getKeyByIndex= (object, index) =>{
    console.log("djfjhdjfhdkff")
    console.log(object)
    console.log(index)
    console.log("djfjhdjfhdkff")
    // Parcourir chaque clé de l'objet
    for (let key in object) {
        // Vérifier si la valeur de la clé correspond à l'indice donné
        if (object[key] === index) {
            // Retourner la clé si la correspondance est trouvée
            return key;
        }
    }
    return null;
}
export const extractNamesToTable = (users) => {
    const namesTable = [];

    // Parcourir chaque utilisateur dans le tableau
    users.forEach((user) => {
        // Récupérer le nom complet (nom et prénom) de l'utilisateur
        const fullName = `${user.firstName} ${user.lastName}`;

        // Ajouter le nom complet au tableau
        namesTable.push(fullName);
    });

    return namesTable;
};
export const getNameAndLastNameByEmail = ( employeesListe,email) => {
    // Recherche l'utilisateur correspondant à l'email donné
    const user = employeesListe.find((data) => data.email === email);

    if (user) {
        // Si l'utilisateur est trouvé, retourne le nom et prénom
        return `${user.firstName} ${user.lastName}`;
    } else {
        // Si aucun utilisateur correspondant n'est trouvé, retourne une chaîne vide ou une indication d'erreur
        return '';
    }
};
export const getNameAndLastNameByAddress = (employee, adresseData) => {
    const result = [];
    // Parcourir chaque entrée dans `titi`
    adresseData.forEach((entry) => {
        // Trouver l'utilisateur correspondant dans `toto` en fonction de l'adresse
        const user = employee.find((user) => user.walletData === entry);

        // Si un utilisateur correspondant est trouvé, récupérer le nom et prénom
        if (user) {
            const fullName = `${user.firstName} ${user.lastName}`;
            result.push(fullName); // Ajouter l'objet avec id et nom complet au résultat
        }
    });

    return result;
};