import React, { useState, useEffect } from 'react';
import '../css/App.css';
import '../css/index.css';
import { useNavigate } from "react-router-dom";
import {
    Backdrop,
    Box,
    Button,
    CircularProgress
} from "@mui/material";
import keycloakConfig from '../config/keycloakConfig';
import {useAccount} from "wagmi";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogActions from "@mui/material/DialogActions";
import Web3 from "web3";
import {Prism as SyntaxHighlighter} from "react-syntax-highlighter";
import {nightOwl} from "react-syntax-highlighter/dist/cjs/styles/prism";
import axios from "axios";
import  CompanyManagement from '../data/abi/CompanyManagement.json'
import {config} from "../config/config"
function SetWallet() {
    const ACCOUNT_ADDRESS =config.address;
    const [openload, setOpenLoad] = useState(true);
    const [initload, setInitload] = useState(true);
    const [userInfo, setUserInfo] = useState(null);
    const navigate = useNavigate();
    const [openDialog, setOpenDialog] = useState(false);
    const { address, isConnecting, isDisconnected } = useAccount();
    const [message, setMessage] = useState("");
    const [employees, setEmployees] = useState([]);
    const [showvc, setShowVc] = useState(false);
    const [valuevc, setValueVC] = useState({});

    const [vcId, setVcId] = useState("");

    useEffect(() => {

        if (initload) {
            setInitload(false);
            return;
        }
        document.title = "Add Wallet";
        setOpenLoad(false);
        if (keycloakConfig.authenticated) {
            showVc();
            setUserInfo(keycloakConfig.idTokenParsed);
            if(address!=undefined){
                console.log(address)
            }else{
                setEmployees([])
            }
        }else{
            navigate('/login');
        }

    }, [initload,address]);
    const handleLogout = async () => {
        if(address!=undefined){
            setMessage("Before exiting the application, please disconnect from your wallet to ensure complete logout.")
            setOpenDialog(true)
        }else {
            await keycloakConfig.logout();
            navigate('/login');
        }

    };
    const addWallet = async (email,address) => {
        if (typeof window.ethereum !== "undefined") {
            const provider = new Web3(window.ethereum);
            const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
            const accounts = await provider.eth.getAccounts();
            const result= await contract.methods.joinCompany(vcId,email).send({from: accounts[0]});

            return result;
        }
    }

    async function addToWallet() {

        if(address==undefined){
            setMessage("Please choose the wallet account you want to add by logging in.")
            setOpenDialog(true)
        }else {
            try {
                const userResult = await addWallet(userInfo.email,address);console.log(userResult);
               if (userResult!=="") {
                   setMessage("The address of your wallet has been successfully saved : " + address + ".");
                   setOpenDialog(true);
                }else{
                   setMessage("Your wallet could not be registered because it is already registered.")
                   setOpenDialog(true)
                }
            } catch (error) {
                setMessage("Your wallet could not be registered.")
                setOpenDialog(true)
                console.error('Erreur lors de la récupération des informations :', error);
            }

        }
    }
    const handleCloseDialog = () => {
        setOpenDialog(false);
    };

    const showVc = async () => {
        const login = await axios.post('http://localhost:7001/wallet-api/auth/login', {
            "type": "email",
            "email": "test@gmail.com",
            "password": "test"
        });

        const idVc = await axios.get('http://localhost:7001/wallet-api/wallet/accounts/wallets', {
            headers: {
                Authorization: `Bearer ${login.data.token}`
            }
        });
        setVcId(idVc.data.wallets[0].id)
    };
    const handleClosec = () => {
        setShowVc(false);
    };
    return (
        <Box >
            {showvc && (
                <div>
                    <Dialog open = {showvc} onClose = {handleClosec} sx = {{ color: '#fff', fontFamily: "Arial", fontSize:"17px", zIndex: (theme) => theme.zIndex.drawer + 1 }}>
                        <DialogTitle style = {{color:'white', background: 'linear-gradient(270deg, #000F8E 25%, #A931F6 129.76%)'}}>Content</DialogTitle>
                        <SyntaxHighlighter language="json" style={nightOwl}  customStyle={{ width: '90%', padding: '30px' }}>
                            {JSON.stringify(valuevc, null, 2)}
                        </SyntaxHighlighter>
                    </Dialog>
                </div>
            )}
            <Dialog
                open={openDialog}
                onClose={handleCloseDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Message
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                         {message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDialog} autoFocus>
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
            <Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={openload}>
                <CircularProgress color="inherit" />
            </Backdrop>

            <div className="row m-0 company-search">
                <div className="row">
                <div className="col-lg-6 d-flex justify-content-start align-items-center">
                    <h3 style={{ color: 'white', marginLeft:'12px'}}>
                        Add wallet page
                    </h3>
                </div>
                <div className='col-lg-6 d-flex justify-content-end align-items-center ' style={{ color: 'white' }}>
                    {userInfo && (
                        <div className="row justify-content-end col ">
                            <div className="col-auto" style={{ marginTop: "20px" }}>
                                <span>{userInfo.email}</span>
                            </div>
                            <div className="col-auto" style={{ marginTop: "10px" }}>
                                <Button style={{ backgroundColor: "#000F8E" }} variant="contained" onClick={handleLogout}>Log Out</Button>
                            </div>
                        </div>
                    )}
                </div>

                    <div className="row company-search m-0">
                <div className="col-lg-6 d-flex justify-content-start align-items-center">
                </div>
                <div className='col-lg-6 d-flex justify-content-end align-items-center ' style={{ color: 'white' }}>
                    {userInfo && (
                        <div className="row justify-content-end col " style={{ marginTop: "10px" }}>
                            <div className="col-auto" >
                                <w3m-network-button />
                            </div>
                            <div className="col-auto">
                                <w3m-button balance={"hide"} />
                            </div>
                        </div>
                    )}
                </div>
                </div>
                </div>
            </div>
            <div sx={{  fontFamily: 'Arial' }} >
                <div className=' row container-fluid corp' style={{ marginTop: '55px' }}>
                    <div style={{ margin: '0 auto' , width: '700px'}} className='col text-center'>
                        <h4>Please click on the button to add a wallet to the organization</h4>
                        <Button style={{backgroundColor:"#000F8E"}} variant="contained" onClick={addToWallet}>Add your address</Button>
                    </div>
                </div>
            </div>
        </Box>
    )
}

export default SetWallet;
