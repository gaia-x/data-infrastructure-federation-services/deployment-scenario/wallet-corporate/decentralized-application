import * as React from 'react';
import axios from "axios";
import '../css/App.css';
import'../css/index.css';
import '../css/Home.css';
import {useEffect, useState } from "react";
import {
    Backdrop,
    Box,
    Button, Pagination, Stack,
    CircularProgress, FormControl, InputLabel, Select, MenuItem

} from "@mui/material";

import Dialog from "@mui/material/Dialog";
import { DataGrid } from '@mui/x-data-grid';
import {useNavigate} from "react-router-dom";
import { getUserInfo } from "../config/keycloak"
import Web3 from "web3";
import CompanyManagement from "../data/abi/CompanyManagement.json";
import { config,rolesConfig } from "../config/config"
import SearchBar from "../component/search";
import {removeLocalStorageItem,getLocalStorageItem,getAllKeys} from "../utils/utils";
import {useAccount} from "wagmi";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContentText from "@mui/material/DialogContentText";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Header from "../component/header";
import {Prism as SyntaxHighlighter} from "react-syntax-highlighter";
import {nightOwl} from "react-syntax-highlighter/dist/cjs/styles/prism";
import  Corporate from '../data/abi/CorporateWallet.json'
function ManageVc() {
    const [openload, setOpenLoad] = useState(true);
    const [initialLoad, setInitialLoad] = useState(true)
    const [currentPage, setCurrentPage] = useState(1);
    const [sizetab, setSizeTab] = useState(1);
    const [searchTerm, setSearchTerm] = useState("");
    const navigate = useNavigate();
    const [employeesData, setEmployeesData] = useState([]);
    const ACCOUNT_ADDRESS =config.address;

    const [openAssignDialog, setAssignDialog] = React.useState(false);
    const { address, isConnecting, isDisconnected } = useAccount();
    const [token, setToken] = useState(getLocalStorageItem('token'));
    const [idwallet, setIdWallet] = useState(getLocalStorageItem('walletId'));
    const [roleData, setRoleData] = useState([]);
    const [role, setRole] = useState('');
    const [roleEmployee, setRoleEmployee] = useState('');
    const [employeeAddress, setEmployeeAddress] = useState('');
    const [openDialog, setOpenDialog] = useState(false);
    const [message, setMessage] = useState("");
    const [selectedRows, setSelectedRows] = useState([]);

    const [showvc, setShowVc] = useState(false);
    const [valuevc, setValueVC] = useState({});

    const [listOfVC, setListOfVC] = useState([]);
    const [listOfVcSelected, setListOfVcSelected] = useState([]);
    const handleValueChange= (event) => {

    };

    useEffect(() => {
        if (initialLoad) {
            setInitialLoad(false);
            return;
        }
        setOpenLoad(true);
        if(isDisconnected){
            logOut()
        }else {
            handleIsHaveRole()
            setRoleData(getAllKeys(rolesConfig))
            if(token && idwallet){
                getVCWallet(token,idwallet)
                getAccountWallet(token);
            }else{
                navigate('/')
            }
        }
        document.title = "Manage VC";
    }, [initialLoad,address,isDisconnected]);

    const handleShareVC = async (vc) => {
        try {
            const response = await axios.post('http://localhost:3002/save-json', vc);
            if (response.data.result) {
                return response.data.result; // Retourner l'URL partagée
            }
        } catch (error) {
            console.error("An error occurred when sharing the VC:", error);
            throw error;
        }
    };

    const handleGetSelectedRows = () => {
        setOpenLoad(true)
        const selectedIds = selectedRows.map((id) => parseInt(id)); // Convertir les identifiants en nombre entier si nécessaire
        const selectedData = rows.filter((row) => selectedIds.includes(row.id));
        setListOfVcSelected(selectedData);
        handleAddIpfsVcs(selectedData);
    };
    const isStorageVcTIpfs = async (Vcs) => {
        const vcNotShare = [];
        try {
            // Fonction pour gérer le partage de chaque VC et la mise à jour des tableaux
            if (typeof window.ethereum !== "undefined") {
                const provider = new Web3(window.ethereum);
                const contract = new provider.eth.Contract(Corporate.abi, ACCOUNT_ADDRESS);
                const accounts = await provider.eth.getAccounts();
                const handleShareAndPush = async (vc) => {
                    try {
                            const isExistVc = await contract.methods.isExist(vc.parsedDocument.id).call()
                            if (!isExistVc) {
                                vcNotShare.push(vc); // Ajouter l'identifiant au tableau idArray
                        }
                    } catch (error) {
                        console.error("An error occurred while sharing VC:", error);
                    }
                };

                // Parcourir chaque objet VC dans le tableau Vcs et partager
                for (const vc of Vcs) {
                    await handleShareAndPush(vc); // Attendre que le partage soit terminé avant de passer au suivant
                }
            }
            setListOfVC(vcNotShare);
            console.log("urlArray:", vcNotShare);
        }catch (error) {
            console.error("An error occurred while sharing VC:", error);
        }
    }
    const getVCWallet = async (tokenData,accountId) => {

        try {
            if (!tokenData) {
                navigate('/');
                return;
            }
            const response = await axios.get('http://localhost:7001/wallet-api/wallet/'+accountId+'/credentials', {
                headers: {
                    Authorization: `Bearer ${tokenData}`
                }
            });
            console.log(response.data)
           await isStorageVcTIpfs(response.data)

        } catch (error) {
            console.error('Erreur lors de la récupération des données:', error);
            setOpenLoad(false);
        }
    };
    const handleAddIpfsVcs = async (Vcs) => {
        const idArray = []; // Tableau pour stocker les identifiants
        const urlArray = []; // Tableau pour stocker les URLs des données partagées
        try {
            // Fonction pour gérer le partage de chaque VC et la mise à jour des tableaux
            if (typeof window.ethereum !== "undefined") {
                const provider = new Web3(window.ethereum);
                const contract = new provider.eth.Contract(Corporate.abi, ACCOUNT_ADDRESS);
                const accounts = await provider.eth.getAccounts();
                const handleShareAndPush = async (vc) => {
                    try {
                        const response = await handleShareVC(vc.data);
                        if (response) {
                                urlArray.push(response); // Ajouter l'URL partagée au tableau urlArray
                                idArray.push(vc.id_vc); // Ajouter l'identifiant au tableau idArray
                        }
                    } catch (error) {
                        console.error("An error occurred while sharing VC:", error);
                    }
                };

                // Parcourir chaque objet VC dans le tableau Vcs et partager
               for (const vc of Vcs) {
                    await handleShareAndPush(vc); // Attendre que le partage soit terminé avant de passer au suivant
                }
                try {
                                if (idArray.length > 0 && urlArray.length > 0 && idArray.length == urlArray.length) {
                                    setMessage("Please accept the addition of new wallet files in ipfs");
                                    const result = await contract.methods.addMultipleCredentialsToCompany(idArray, urlArray).send({from: accounts[0]});
                                    window.location.reload();
                                }
                } catch (error) {
                    setMessage("The file has not been added to ipfs.");
                    setOpenDialog(true)
                    setOpenLoad(false)
                    console.error("An error occurred while sharing VC:", error);
                }
            }
        }catch (error) {
            setOpenLoad(false)
            console.error("An error occurred while sharing VC:", error);
        }
    };
    const getRole=(isAdmin)=> {
        switch (isAdmin) {
            case false:
                navigate('/');
        }
    }
    const handleIsHaveRole = async () => {
        const provider = new Web3(window.ethereum);
        const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
        const employeesOfCompany= await contract.methods.employees(address).call();
        getRole(employeesOfCompany['isAdmin'])
    }



    const logOut= async () => {

        try {
            removeLocalStorageItem("token");
            removeLocalStorageItem("walletId");
            navigate('/');
        } catch (error) {
            console.error('Erreur lors de la déconnexion:', error);
        }
    };
    const getAccountWallet = async (tokenData) => {
        try {
            if (!tokenData) {
                navigate('/');
                return;
            }
            const usersInfo=await getUserInfo();
            filterEmployeesData(usersInfo,idwallet)
                .then(filteredEmployeesData => {
                    const filteredData = filteredEmployeesData.filter(item => item.walletData);
                    setEmployeesData(filteredData);
                }).catch(error => {
                console.error("Une erreur s'est produite :", error);
            });
            setOpenLoad(false);
        } catch (error) {
            console.error('Erreur lors de la récupération des données:', error);
            setOpenLoad(false);
        }
    };
    async function filterEmployeesData(usersInfo,accountId) {
        const provider = new Web3(window.ethereum);
        const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
        const accounts = await provider.eth.getAccounts();
        const employeesOfCompany= await contract.methods.getEmployeesOfMyCompany().call({from: address});
        const emailPromises = usersInfo.map(async item => {
            if (item.hasOwnProperty("email")) {
                const index = employeesOfCompany['1'].findIndex(employee => employee.email === item.email);
                console.log(index)
                if (index !== -1) {
                    return { ...item, walletData: employeesOfCompany['0'][index], role: employeesOfCompany['1'][index]['role'] };
                }
            } else {
                return null;
            }
        });


        const emailResults = await Promise.all(emailPromises);

        return emailResults.filter(Boolean); // Filtrer les résultats pour supprimer les valeurs null
    }

    const getVC = () => {
        navigate('/allvc');
    };

    const handlePageChange = (event, page) => {
        setCurrentPage(page);
    };

    const columns = [
        { field: 'id', headerName: 'ID', width: 100 },
        { field: 'id_vc', headerName: 'ID VC', width: 350 },
        { field: 'typeOfVc', headerName: 'TYPE OF VC', width: 300 },
        { field: 'issuanceDate', headerName: 'DATE ISSUANCE', width: 200 },
        { field: 'issuer', headerName: 'ISSUER', width: 400 },
        { field: 'valideFrom', headerName: 'DATE OF VALIDITY', width: 200 },
        {
            field: 'action',
            headerName: 'Action', headerClassName: 'column-text',
            description: 'Gestion des action sur les VC',
            sortable: false,
            width: 300, headerAlign: 'center', align: 'center',
            renderCell: (params) => (
                <div style={{ display: 'flex', gap: '10px'}}>
                    <Button style={{backgroundColor:'#000F8E'}} variant="contained"  onClick={() => handleShowVC(params.row.id)}>Show VC</Button>
                </div>

            ),
        }
    ];
    function rowValue(data) {
        if (!Array.isArray(data)) {
            return [];
        }
        const row = data.map((item, index) => {
            return {
                id: index,
                id_vc: item.id,
                typeOfVc: item.parsedDocument.type,
                issuer: item.parsedDocument.issuer,
                issuanceDate: item.parsedDocument.issuanceDate,
                valideFrom: item.parsedDocument.validFrom,
                data: item.parsedDocument,

            }
        });
        return row;
    }
    const rows = rowValue(listOfVC);
    const handleChangeRole = (event) => {
        setRole(event.target.value);
    };

    const handleCloseFormDialog = () => {
        setAssignDialog(false);
        setRole("")
    };
    const handleRole= async () => {
        try{
            //mettre à jour le role de employee
            setAssignDialog(false);
            const provider = new Web3(window.ethereum);
            const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
            const accounts = await provider.eth.getAccounts();
            const updateRoleEmployees= await contract.methods.updateEmployeeRole(employeeAddress,rolesConfig[role]).send({from: accounts[0]});
            window.location.reload();
        } catch (error) {
            setMessage("your role update has not been taken into account, please try again.")
            setOpenDialog(true)
            console.error(error);
        }
    };
    const handleCloseDialog = () => {
        setOpenDialog(false);
    };

    const handleSelectionChange = (selection) => {
        setSelectedRows(selection)
    };
    const handleShowVC = (vcValue) => {console.log(vcValue)
            setShowVc(true);
            setValueVC(listOfVC[vcValue].parsedDocument)
    };
    const handleClosec = () => {
        setShowVc(false);
    };
    return (
        <Box className='corp'>
            {showvc && (
                <div >
                    <Dialog open = {showvc} onClose = {handleClosec} sx = {{ color: '#fff', fontSize:"17px", zIndex: (theme) => theme.zIndex.drawer + 1 }} >
                        <DialogTitle style = {{color:'white', background: 'linear-gradient(270deg, #000F8E 25%, #A931F6 129.76%)'}}>Content</DialogTitle>
                        <SyntaxHighlighter language="json" style={nightOwl}  customStyle={{ width: '100%' }}>
                            {JSON.stringify(valuevc, null, 2)}
                        </SyntaxHighlighter>
                    </Dialog>
                </div>
            )}
            <Dialog
                open={openDialog}
                onClose={handleCloseDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Message
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDialog} autoFocus>
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog
                open={openAssignDialog}
                onClose={handleCloseFormDialog}>
                <DialogTitle>Add role</DialogTitle>
                <DialogContent>
                    <div>
                        <FormControl sx={{ m: 1, width: 500 }}>
                            <InputLabel id="demo-simple-select-label">choosing a role</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={role}
                                onChange={handleChangeRole}>

                                {roleData.map((name) => (
                                    <MenuItem key={name} value={name}>
                                        {name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseFormDialog}>Cancel</Button>
                    <Button onClick={handleRole}>ADD</Button>
                </DialogActions>
            </Dialog>
            <Header/>
            <div className="row company-search container-fluid">
                <div className="col-lg-6 d-flex justify-content-start align-items-center">
                    <h3 style={{ color: 'white', marginLeft:'12px' }}>
                        List of VCs not deployed on IPFS
                    </h3>
                </div>
                <div className='col-lg-6  d-flex justify-content-end align-items-center' style={{ color: 'white', }}>
                    <SearchBar handleValueChange={handleValueChange} />
                </div>
            </div>
            <Backdrop
                sx = {{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open = {openload}>
                <CircularProgress color = "inherit" />
            </Backdrop>
            <div>
                <div   style={{ marginRight:'2rem', marginTop:'6px', marginLeft:'2rem'}}>
                    <Button variant="contained" style={{ backgroundColor:'#000F8E'}} onClick={() => getVC()}>Home</Button>
                </div>
                <div className='m-3'>
                {   (selectedRows.length !== 0 ?
               <Button variant="contained"  style={{ backgroundColor:'#000F8E'}}  onClick={handleGetSelectedRows}>Add Vc to IPFS</Button>
               :
                        <Button variant="contained" disabled={"true"} style={{ backgroundColor: '#000F8E', color: 'white', opacity:'0.5' }} >Add Vc to IPFS</Button>
                )}
                </div>
                <DataGrid
                    rows={rows}
                    columns={columns}
                    initialState={{
                        pagination: {
                            paginationModel: { page: 0, pageSize: 100 },
                        },
                    }}
                    pageSizeOptions={[0, 100]}
                    checkboxSelection
                  onRowSelectionModelChange={handleSelectionChange}
                />
                <Stack spacing={2} sx={{mt:'20px'}}>
                    <Pagination count={sizetab} sx={{marginLeft:'60px',
                        '& .MuiPaginationItem-page.Mui-selected': {
                            color: 'white',
                            backgroundColor:'#000F8E'
                        },
                        '& .MuiPaginationItem-page': {
                            color: '#000F8E',
                        }}} variant="outlined" shape="rounded"  page={currentPage}
                                onChange={handlePageChange} />
                </Stack>
            </div>
        </Box>


    )
}


export default ManageVc;
