import React, { useState, useEffect } from 'react';
import '../css/App.css';
import '../css/index.css';
import'../css/Home.css';
import { useNavigate } from "react-router-dom";
import {
    Backdrop,
    Box,
    Button,
    CircularProgress
} from "@mui/material";
import keycloakConfig from '../config/keycloakConfig';

function LoginPage() {

    const [openload, setOpenLoad] = useState(true);
    const [initialLoad, setInitialLoad] = useState(true);
    const navigate = useNavigate();

    async function loginToKeycloak() {
        if(!keycloakConfig.authenticated) {
            await keycloakConfig.login({idpHint: 'oidc'});
        }else {
            navigate("/addwallet")
        }
    }
    useEffect(() => {

        if (initialLoad) {
            setInitialLoad(false);
            return;
        }
        document.title = "Add Wallet";
        if(keycloakConfig.authenticated) {
            navigate('/addwallet');
        }
        setOpenLoad(false);
    }, [initialLoad]);

    return (
        <Box>
            <Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={openload}>
                <CircularProgress color="inherit" />
            </Backdrop>

            <div className="row company-search container-fluid">
                <div className="col-lg-6 d-flex justify-content-start align-items-center">
                    <h1 style={{ color: 'white', marginLeft:'12px' }}>
                        Welcome to the login page
                    </h1>
                </div>

            </div>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: '100vh' }}>
                <div style={{ marginTop: '6px', textAlign: 'center' }} className='row container-fluid'>
                    <div>
                        <h4 className='me-4'>Please click on the login button below to provide your login and password.</h4>
                        <Button variant="contained" style={{ backgroundColor: "#000F8E" }} onClick={loginToKeycloak}>Connexion</Button>
                    </div>
                </div>
            </div>


        </Box>
    )
}

export default LoginPage;
