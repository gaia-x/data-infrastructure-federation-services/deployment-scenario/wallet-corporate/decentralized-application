import * as React from 'react';
import axios from "axios";
import '../css/App.css';
import'../css/index.css';
import '../css/Home.css';
import {useEffect, useState,useRef } from "react";
import testVC from '../image/toto.json'
import {
    Backdrop,
    Box,
    Button,
    Pagination,
    Stack,
    CircularProgress,
    Checkbox,
    Tooltip, FormControlLabel, FormGroup,

} from "@mui/material";

import Dialog from "@mui/material/Dialog";
import { DataGrid } from '@mui/x-data-grid';
import {useNavigate} from "react-router-dom";
import { getUserInfo } from "../config/keycloak"
import Web3 from "web3";
import CompanyManagement from "../data/abi/CompanyManagement.json";
import { config,rolesConfig } from "../config/config"
import SearchBar from "../component/search";

import {removeLocalStorageItem,getLocalStorageItem,getAllKeys,getKeyByIndex} from "../utils/utils";
import {useAccount} from "wagmi";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContentText from "@mui/material/DialogContentText";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import {Prism as SyntaxHighlighter} from "react-syntax-highlighter";
import {nightOwl} from "react-syntax-highlighter/dist/cjs/styles/prism";
function ManageVcEmployee({ nameOfGroupToManage }) {
    const [openload, setOpenLoad] = useState(true);
    const [limit, setLimit] = useState(10);
    const [initialLoad, setInitialLoad] = useState(true)
    const [currentPage, setCurrentPage] = useState(1);
    const [sizetab, setSizeTab] = useState(1);
    const navigate = useNavigate();
    const [employeesData, setEmployeesData] = useState([]);
    const ACCOUNT_ADDRESS =config.address;
    const [openAssignDialog, setAssignDialog] = React.useState(false);
    const { address, isConnecting, isDisconnected } = useAccount();
    const [token, setToken] = useState(getLocalStorageItem('token'));
    const [idwallet, setIdWallet] = useState(getLocalStorageItem('walletId'));
    const [roleData, setRoleData] = useState([]);
    const [roleEmployee, setRoleEmployee] = useState('');
    const [openDialog, setOpenDialog] = useState(false);
    const [message, setMessage] = useState("");
    const [selectedRows, setSelectedRows] = useState([]);
    const [showvc, setShowVc] = useState(false);
    const [valuevc, setValueVC] = useState({});

    const [listOfVC, setListOfVC] = useState([]);
    const [checkedValues, setCheckedValues] = useState({
        Read: false,
        Consumption: false,
        Presentation: false,
        Import: false,
    });
    const handleValueChange= (event) => {

    };

    useEffect(() => {
        if (initialLoad) {
            setInitialLoad(false);
            return;
        }
        setOpenLoad(true);
        if(isDisconnected){
            logOut()
        }else {
            setListOfVC([testVC])
            handleIsHaveRole()
            setRoleData(getAllKeys(rolesConfig))
            if(token && idwallet){
                getAccountWallet(token);
            }else{
                navigate('/')
            }
        }
        document.title = "Manage VC Employee";
    }, [initialLoad,address,isDisconnected]);

    const getRole=(isAdmin)=> {
        switch (isAdmin) {
            case false:
                navigate('/');
        }
    }
    const handleIsHaveRole = async () => {
        const provider = new Web3(window.ethereum);
        const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
        const employeesOfCompany= await contract.methods.employees(address).call();
        getRole(employeesOfCompany['isAdmin'])
    }

    const logOut= async () => {

        try {
            removeLocalStorageItem("token");
            removeLocalStorageItem("walletId");
            navigate('/');
        } catch (error) {
            console.error('Erreur lors de la déconnexion:', error);
        }
    };
    const getAccountWallet = async (tokenData) => {
        try {
            if (!tokenData) {
                navigate('/');
                return;
            }
            const usersInfo=await getUserInfo();
            filterEmployeesData(usersInfo,idwallet)
                .then(filteredEmployeesData => {
                    const filteredData = filteredEmployeesData.filter(item => item.walletData);
                    setEmployeesData(filteredData);
                }).catch(error => {
                console.error("Une erreur s'est produite :", error);
            });
            setOpenLoad(false);
        } catch (error) {
            console.error('Erreur lors de la récupération des données:', error);
            setOpenLoad(false);
        }
    };
    async function filterEmployeesData(usersInfo,accountId) {
        const provider = new Web3(window.ethereum);
        const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
        const accounts = await provider.eth.getAccounts();
        const employeesOfCompany= await contract.methods.getEmployeesOfMyCompany().call({from: address});
        const emailPromises = usersInfo.map(async item => {
            if (item.hasOwnProperty("email")) {
                const index = employeesOfCompany['1'].findIndex(employee => employee.email === item.email);
                console.log(index)
                if (index !== -1) {
                    return { ...item, walletData: employeesOfCompany['0'][index], role: employeesOfCompany['1'][index]['role'] };
                }
            } else {
                return null;
            }
        });
        const emailResults = await Promise.all(emailPromises);
        return emailResults.filter(Boolean); // Filtrer les résultats pour supprimer les valeurs null
    }

    const handleSearch = () => {
        // Function to search user
    };
    const handlePageChange = (event, page) => {
        setCurrentPage(page);
    };

    const columns = [
        { field: 'id', headerName: 'ID', width: 100 },
        { field: 'id_vc', headerName: 'ID VC', width: 350 },
        {
            field: 'typeOfVc',
            headerName: 'TYPE OF VC',
            flex: 1,
            renderCell: (params) => (
                <Tooltip
                    title={`Issuer: ${params.row.issuer || 'N/A'}, Issuance: ${params.row.issuanceDate || 'N/A'}, Validity: ${params.row.valideFrom || 'N/A'}`}
                >
                    <span>{params.row.typeOfVc}</span>
                </Tooltip>
            )
        },
        { field: 'groupe', headerName: 'GROUPE', width: 200 ,
            renderCell: () => (
                <div>
                    {nameOfGroupToManage}
                </div>

            )},
        {
            field: 'liste_of_Right',
            headerName: 'LIST OF RIGHTS', headerClassName: 'column-text',
            description: 'List of VC rights',
            sortable: false, width: 320 , headerAlign: 'center', align: 'center',
            renderCell: () => (
                <div>
                <span>
                    Read, consumption, Presentation, Import.
                    </span>
                </div>
            ),
        },
        {
            field: 'action',
            headerName: 'ACTION', headerClassName: 'column-text',
            description: 'Managing Vcs rights',
            sortable: false,
            flex:1 , headerAlign: 'center', align: 'center',
            renderCell: (params) => (
                <div>
                    <Button style={{backgroundColor:'#000F8E'}} variant="contained"  onClick={() => handleShowVC(params.row.id)}>Show VC</Button>
                    <Button style={{backgroundColor:'#000F8E',marginLeft:'15px'}} variant="contained"  onClick={() => handleRevokeRight(params.row.id)}>Revoke rights</Button>
                </div>
            ),
        }
    ];
    function rowValue(data) {
        if (!Array.isArray(data)) {
            return [];
        }
        const row = data.map((item, index) => {
            return {
                id: index,
                id_vc: item.id,
                typeOfVc: item.parsedDocument.type,
                issuer: item.parsedDocument.issuer,
                issuanceDate: item.parsedDocument.issuanceDate,
                valideFrom: item.parsedDocument.validFrom,
                data: item.parsedDocument,
            }
        });
        return row;
    }
    const rows = rowValue(listOfVC);

    const handleCloseFormDialog = () => {
        setAssignDialog(false);
        setCheckedValues( resetKeysToFalse(checkedValues))
    };

    const handleCloseDialog = () => {
        setOpenDialog(false);
    };
    const handleSelectionChange = (selection) => {
        console.log(selection)
        setSelectedRows(selection)
    };
    const handleShowVC = (vcValue) => {
            setShowVc(true);
            setValueVC(listOfVC[vcValue].parsedDocument)
    };
    const handleClosec = () => {
        setShowVc(false);
    };
    const handleRevokeRight = () => {
        setAssignDialog(true);
    };
    const  checkIfAnyTrue= (object)=> {
        const values = Object.values(object);
        const hasTrueValue = values.includes(true);
        if (!hasTrueValue) {
            return false;
        }
        return true;
    }
   const resetKeysToFalse= (object)=> {
        // Parcourir chaque clé de l'objet
        for (const key in object) {
            // Mettre la valeur de chaque clé à false
            object[key] = false;
        }
        return object;
    }
    const RevokeRight = async () => {
        try {
            if(checkIfAnyTrue(checkedValues)) {
                setAssignDialog(false);
                setOpenLoad(true);
                const provider = new Web3(window.ethereum);
                const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
                const accounts = await provider.eth.getAccounts();
                const employeesOfCompany = await contract.methods.getEmployeesOfMyCompany().send({from: address});

                if (employeesOfCompany) {
                   setCheckedValues(resetKeysToFalse(checkedValues))
                    setMessage("The rights have been removed");
                    setOpenDialog(true);
                    setOpenLoad(false);
                } else {
                    setOpenLoad(false);
                }
            }else {
                setMessage("Please select at least one right to delete");
                setOpenDialog(true);
            }

        }catch (error) {
            setMessage("Error when deleting rights.")
            setOpenDialog(true);
            setOpenLoad(false);
            setCheckedValues( resetKeysToFalse(checkedValues))
            console.log(error)
        }
    };
    const handleCheckboxChange = (event) => {
        const { name, checked } = event.target;
        setCheckedValues({ ...checkedValues, [name]: checked });
    };
    return (
        <Box className='corp'>
            {showvc && (
                <div >
                    <Dialog open = {showvc} onClose = {handleClosec} sx = {{ color: '#fff', fontSize:"17px"}} keepMounted>
                        <DialogTitle style = {{color:'white', background: 'linear-gradient(270deg, #000F8E 25%, #A931F6 129.76%)'}}>Content</DialogTitle>
                        <SyntaxHighlighter language="json" style={nightOwl}  customStyle={{ width: '100%' }}>
                            {JSON.stringify(valuevc, null, 2)}
                        </SyntaxHighlighter>
                    </Dialog>
                </div>
            )}
            <Dialog
                open={openDialog}
                onClose={handleCloseDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogTitle id="alert-dialog-title">
                    Message
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDialog} autoFocus>
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog
                open={openAssignDialog}
                onClose={handleCloseFormDialog}
                fullWidth={'sm'}
                maxWidth={'sm'}>
                <DialogTitle>Choosing a right to revoked</DialogTitle>
                <DialogContent>
                    <div>
                        <FormGroup>
                            <FormControlLabel
                                control={<Checkbox checked={checkedValues.Read} onChange={handleCheckboxChange} name="Read"   style={{ color: '#000F8E' }} />}
                                label="Read"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={checkedValues.Consumption} onChange={handleCheckboxChange} name="Consumption" style={{ color: '#000F8E'}} />}
                                label="Consumption"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={checkedValues.Presentation} onChange={handleCheckboxChange} name="Presentation" style={{ color: '#000F8E'}} />}
                                label="Presentation"
                            />
                            <FormControlLabel
                                control={<Checkbox disabled={'true'} checked={checkedValues.Import} onChange={handleCheckboxChange} name="Import" style={{ color:'#000F8E'}} />}
                                label="Import"
                            />
                        </FormGroup>
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseFormDialog}>Cancel</Button>
                    <Button onClick={RevokeRight}>Revoked</Button>
                </DialogActions>
            </Dialog>
            <div className="row company-search container-fluid">
                <div className="col-lg-6 d-flex justify-content-start align-items-center">
                    <h3 style={{ color: 'white', marginLeft:'12px' }}>
                        Manage the rights
                    </h3>
                </div>
                <div className='col-lg-6  d-flex justify-content-end align-items-center' style={{ color: 'white' }}>
                    <SearchBar handleValueChange={handleValueChange} />
                </div>
            </div>
            <Backdrop
                sx = {{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open = {openload}>
                <CircularProgress color = "inherit" />
            </Backdrop>
            <div>
                <div style={{ margin:'10px'}}>
                <DataGrid
                    rows={rows}
                    columns={columns}
                    initialState={{
                        pagination: {
                            paginationModel: { page: 0, pageSize: 100 },
                        },
                    }}
                    pageSizeOptions={[0, 100]}
                  onRowSelectionModelChange={handleSelectionChange}
                />

                <Stack spacing={2} sx={{mt:'20px'}}>
                    <Pagination count={sizetab} sx={{marginLeft:'60px',
                        '& .MuiPaginationItem-page.Mui-selected': {
                            color: 'white',
                            backgroundColor:'#000F8E'
                        },
                        '& .MuiPaginationItem-page': {
                            color: '#000F8E',
                        }}} variant="outlined" shape="rounded"  page={currentPage}
                                onChange={handlePageChange} />
                </Stack>
                </div>
            </div>
        </Box>
    )
}


export default ManageVcEmployee;
