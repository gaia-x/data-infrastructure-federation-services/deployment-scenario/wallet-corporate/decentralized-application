import React, {useEffect, useState} from 'react';
import {Backdrop, Button, CircularProgress} from '@mui/material';
import Web3 from 'web3';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import {useNavigate} from 'react-router-dom';
import axios from "axios";
import CompanyManagement from '../data/abi/CompanyManagement.json'
import {config, rolesConfig} from "../config/config"
import {getKeyByIndex, setLocalStorageItem} from "../utils/utils";
import {useAccount} from "wagmi";
import '../css/globals.css'
import Header from "../component/header";

const Connect= () => {
    const [message, setMessage] = useState("");
    const [token, setToken] = useState("");
    const [walletExist, setWalletExist] = useState(false);
    const [walletId, setWalletId] = useState("");
    const ACCOUNT_ADDRESS =config.address;
    const [open, setOpen] = useState(false);
    const [openDialog, setOpenDialog] = useState(false);
    const navigate = useNavigate();
    const [initload, setInitload] = useState(true);
    const { address, isConnecting, isDisconnected } = useAccount();

    useEffect(() => {
        if (initload) {
            setInitload(false);
            return;
        }
        setOpen(true)
        handleIsExistWallet();
        document.title = "Login";
    }, [initload,address,walletExist]);

    const getRole=(isAdmin)=> {
        switch (isAdmin) {
            case true:
                navigate('/allvc');
                default:
                setOpenDialog(true)
                setMessage("You do not have the necessary rights to access this area")

        }
    }
    const handleIsHaveRole = async () => {
        const provider = new Web3(window.ethereum);
        const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
        const employeesOfCompany= await contract.methods.employees(address).call();
        getRole(employeesOfCompany['isAdmin'])
    }
    const handleIsExistWallet = async () => {
        try {
        const tokenValue = await handleConnectWallet();
        const idWallet = await getIdWallet(tokenValue);
            setToken(tokenValue);
            setWalletId(idWallet);
            setLocalStorageItem("token",tokenValue)
            setLocalStorageItem("walletId",idWallet)
            const userResult = await handleIsWalletExist(idWallet);

            if(userResult=="" && address!=undefined ){
                setWalletExist(true);
               await handleIsHaveRole();
            }else if(userResult==""  && address==undefined){
                setWalletExist(true);
            }else if( userResult!=="" && address!=undefined  ){
                setWalletExist(false);
                setMessage("Please log in with your wallet to save it.")
                setOpenDialog(true);
            }
            setOpen(false);
        } catch (error) {
            console.log('Erreur lors de la récupération des informations de connexion:', error);
        }
    }
    const handleSignIn = async () => {
        try {
            if(!walletExist && address!=undefined){
                const userResult = await addWallet(walletId);
                if (userResult!=="" && userResult[0]) {
                   navigate('/allvc');
                }
            }else if(!walletExist && address==undefined){
                setMessage("Please click to connect your wallet")
                setOpenDialog(true);
            }

        } catch (error) {
            // Gérer les erreurs
            console.error('Erreur lors de la récupération des informations de connexion:', error);
        }
    };

    const addWallet = async (accountId) => {
        if (typeof window.ethereum !== "undefined") {
                const provider = new Web3(window.ethereum);
                const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
                const accounts = await provider.eth.getAccounts();
                await contract.methods.registerCompany(accountId,config.companyName).send({from: accounts[0]});
                return accounts;
            }
        }

    const getIdWallet = async (tokenData) => {
        const response = await axios.get('http://localhost:7001/wallet-api/wallet/accounts/wallets', {
            headers: {
                Authorization: `Bearer ${tokenData}`
            }
        });
        return response.data.wallets[0].id;
    }

    const getWallet = async (accountId) => {
        // If MetaMask exists
        if (typeof window.ethereum !== "undefined") {
            const provider = new Web3(window.ethereum);
            const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);

            try {
                const data = await contract.methods.getRegisteredCompanies().call();
                        if (data["0"].length!==0 && data["0"][0] === accountId) {
                            return true; // L'adresse est déjà présente
                        }
                    return false; // L'adresse n'est pas présente
            } catch (error) {
                console.log("Error: ", error);
            }
        }
    };

    const handleIsWalletExist = async (accountId) => {
        if (typeof window.ethereum !== "undefined") {
          if(await getWallet(accountId)){
               return "";
           }else {
                return null;
           }
        }
    }
    const handleConnectWallet = async () => {
        const login = await axios.post('http://localhost:7001/wallet-api/auth/login', {
            "type": "email",
            "email": config.email,
            "password": config.password
        });
        return login.data.token;
    }
    const handleClose = () => {
        setOpen(false);
    };
    const handleCloseDialog = () => {
        setOpenDialog(false);
    };

    return (
        <div className="row " style={{ height: "100vh" }}>
            <Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={open}
                onClick={handleClose}
            >
                <CircularProgress color="inherit" />
            </Backdrop>
            <Dialog
                open={openDialog}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Message
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDialog} autoFocus>
                       OK
                    </Button>
                </DialogActions>
            </Dialog>
            <Header/>

            <div className="row h-100 login-wrapper">
                <div className="col d-flex align-items-center justify-content-center">
                    <div className="row row-cols-1 ">
                       { !walletExist ? ( <div>
                                                    <div className={`card shadow-sm`}>
                               <div className="card-body">
                                   <h5 className="title">Register your wallet as a corporate wallet</h5>
                                   <Button variant="contained" style={{backgroundColor:"#000F8E"}} className=" me-3 mt-3"  onClick={handleSignIn}>Register your wallet</Button>
                               </div>
                           </div>
                       </div>) : (
                           <div>
                               <div className={`card shadow-sm`}>
                                   <div className="card-body">
                                       <h5 className="title-card">Corporate Wallet</h5>
                                           <div className="description">Welcome to corporate Wallet, the simple way to access and manage your work credential. <br/><br/>
                                               Please login with your wallet.</div>
                                   </div>
                               </div>
                           </div>
                       ) }
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Connect;
