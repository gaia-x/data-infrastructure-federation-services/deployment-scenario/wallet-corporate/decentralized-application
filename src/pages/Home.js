import * as React from 'react';
import axios from "axios";
import '../css/App.css';
import'../css/index.css';
import '../css/Home.css';
import {useEffect, useState} from "react";
import ManageVcEmployee from "./ManageVcEmployee";
import {
    Backdrop,
    Box,
    Button,
    Pagination,
    Stack,
    CircularProgress,
    FormControl,
    InputLabel,
    Select,
    MenuItem, FormGroup, FormControlLabel, Checkbox
} from "@mui/material";

import Dialog from "@mui/material/Dialog";
import { DataGrid } from '@mui/x-data-grid';
import {useNavigate} from "react-router-dom";
import { getUserInfo } from "../config/keycloak"
import Web3 from "web3";
import CompanyManagement from "../data/abi/CompanyManagement.json";
import PermissionManager from "../data/abi/PermissionManager.json";
import { config,rolesConfig } from "../config/config"
import SearchBar from "../component/search";
import {removeLocalStorageItem,getLocalStorageItem,getAllKeys,getKeyByIndex} from "../utils/utils";
import {useAccount} from "wagmi";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContentText from "@mui/material/DialogContentText";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Header from "../component/header";
import AddRight from "../component/AddRight";
function Home() {
    const [openload, setOpenLoad] = useState(true);
    const [openAddRightDialog, setOpenAddRightDialog] = useState(false);
    const [limit, setLimit] = useState(10);
    const [initialLoad, setInitialLoad] = useState(true)
    const [currentPage, setCurrentPage] = useState(1);
    const [sizetab, setSizeTab] = useState(1);
    const [searchTerm, setSearchTerm] = useState("");
    const navigate = useNavigate();
    const [employeesData, setEmployeesData] = useState([]);
    const ACCOUNT_ADDRESS =config.address;

    const [openAssignDialog, setAssignDialog] = useState(false);
    const [openManageRight, setOpenManageRight] = useState(false);
    const { address, isConnecting, isDisconnected } = useAccount();
    const [token, setToken] = useState(getLocalStorageItem('token'));
    const [idwallet, setIdWallet] = useState(getLocalStorageItem('walletId'));
    const [roleData, setRoleData] = useState([]);
    const [role, setRole] = useState('');
    const [roleEmployee, setRoleEmployee] = useState(false);
    const [employeeAddress, setEmployeeAddress] = useState('');
    const [walletAddress, setWalletAddress] = useState('');
    const [openDialog, setOpenDialog] = useState(false);
    const [message, setMessage] = useState("");
    const [rowData, setRowData] = useState([]);
    const [checkedValues, setCheckedValues] = useState({
        Read: false,
        Consumption: false,
        Presentation: false,
        Import: false,
        WalletCorporate: false,
    });
    const handleValueChange= (event) => {

    };
    const handleIsHaveRole = async () => {
        try {
            if (typeof window.ethereum !== "undefined") {
                const provider = new Web3(window.ethereum);
                const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
                const employeesOfCompany = await contract.methods.employees(address).call();
                const roleEmployeeValue =  employeesOfCompany['isAdmin']
                setRoleEmployee(roleEmployeeValue);
                getRole(roleEmployeeValue)

            }
        }catch (error){
                console.log(error)
            }
    }
    const getRole=(isAdmin)=> {
        if(!isAdmin) {
                navigate('/');
        }
    }

    useEffect(() => {
        if (initialLoad) {
            setInitialLoad(false);
            return;
        }

        if(isDisconnected){
            logOut()
        }else {
            handleIsHaveRole()
            setRoleData(getAllKeys(rolesConfig))
            if(token && idwallet){
                getAccountWallet(token);
            }else{
                navigate('/')
            }
            setOpenLoad(true);
        }
        document.title = "Home";
    }, [initialLoad,address,isDisconnected]);

    const logOut= async () => {

        try {
            removeLocalStorageItem("token");
            removeLocalStorageItem("walletId");
            navigate('/');
        } catch (error) {
            console.error('Erreur lors de la déconnexion:', error);
        }
    };
    const getAccountWallet = async (tokenData) => {
        try {
            if (!tokenData) {
                navigate('/');
                return;
            }
            const usersInfo=await getUserInfo();
            filterEmployeesData(usersInfo,idwallet)
                .then(async filteredEmployeesData => {
                    const filteredData = filteredEmployeesData.filter(item => item.walletData);

                    const listEmployees=await rowValue(filteredData)
                    setRowData(listEmployees)
                    setEmployeesData(filteredData);
                }).catch(error => {
                console.error("Une erreur s'est produite :", error);
            });
            setOpenLoad(false);
        } catch (error) {
            console.error('Erreur lors de la récupération des données:', error);
            setOpenLoad(false);
        }
    };
    async function filterEmployeesData(usersInfo,accountId) {
        const provider = new Web3(window.ethereum);
        const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
        const employeesOfCompany= await contract.methods.getEmployeesOfMyCompany().call({from: address});
        const emailPromises = usersInfo.map(async item => {
            if (item.hasOwnProperty("email")) {
                const index = employeesOfCompany['1'].findIndex(employee => employee.email === item.email);
                if (index !== -1) {
                  return { ...item, walletData: employeesOfCompany['0'][index], role: employeesOfCompany['1'][index]['isAdmin'] };
                }
            } else {
                return null;
            }
        });
        const emailResults = await Promise.all(emailPromises);
        return emailResults.filter(Boolean); // Filtrer les résultats pour supprimer les valeurs null
    }


    const handleSearch = () => {
        // Function to search user
    };
    const getVC = () => {
        navigate('/allvc');
    };

    const handleInputChange = (event) => {
        setSearchTerm(event.target.value);
    };

    const handlePageChange = (event, page) => {
        setCurrentPage(page);
    };

    const columns = [
        { field: 'id', headerName: 'ID', width: 100 },
        { field: 'firstName', headerName: 'First Name', width: 150 },
        { field: 'lastName', headerName: 'Last Name', width: 150 },
        { field: 'email', headerName: 'Email', width: 300 },
        { field: 'group', headerName: 'Group', width: 150 },
        { field: 'phone', headerName: 'Phone', width: 150 },
        { field: 'address', headerName: 'Address', width: 200 },
        { field: 'company', headerName: 'Company', width: 150 },
        { field: 'statut', headerName: 'Statut',
            sortable: false,
            width: 100, headerAlign: 'center', align: 'center',
            renderCell: (params) => (
                <div style={{ display: 'flex', gap: '10px', alignItems: 'center' }}>
                    <span>{params.row.statut}</span>
                    {params.row.statut === 'actif' ? (
                        <div style={{ width: '20px', height: '20px', backgroundColor: 'green', borderRadius: '50%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <span style={{ color: 'white' }}>A</span>
                        </div>
                    ) : (
                        <div style={{ width: '20px', height: '20px', backgroundColor: 'red', borderRadius: '50%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <span style={{ color: 'white' }}>I</span>
                        </div>
                    )}
                </div>
            )
        },
        { field: 'walletEmployee', headerName: 'Wallet Employee', width: 400 },
        { field: 'role', headerName: 'Rôle', width: 200 },
        {
            field: 'liste_of_Right',
            headerName: 'LIST OF RIGHTS', headerClassName: 'column-text',
            description: 'List of VC rights',
            sortable: false, width: 320 , headerAlign: 'center', align: 'center',
            renderCell: (params) => (
                <div>
                    {params.row.listRight}
                </div>
            ),
        },
        {
            field: 'action',
            headerName: 'Action',
            headerClassName: 'column-text',
            description: '',
            sortable: false,
            width: 700,
            headerAlign: 'center',
            align: 'center',
            renderCell: (params) => (
                roleEmployee ? (
                    <div>
                        <Button
                            variant="contained"
                            style={{
                                fontSize: '12px',
                                width: '200px',
                                backgroundColor: '#000F8E',
                                color: 'white',
                            }}
                            onClick={() => handleAddRole(params.row.walletEmployee)}
                        >
                            Add role
                        </Button>
                        <Button
                            variant="contained"
                            style={{
                                fontSize: '12px',
                                marginLeft:"10px",
                                width: '200px',
                                backgroundColor: '#000F8E',
                                color: 'white'
                            }}
                            onClick={() => handleAddRight(params.row.walletEmployee)}>
                            Add rights
                        </Button>
                        <Button
                            variant="contained"
                            style={{
                                fontSize: '12px',
                                width: '200px',
                                backgroundColor: '#000F8E',
                                color: 'white',
                                marginLeft:"10px"
                            }}
                           onClick={handleRightInfos}
                        >
                            Manage right
                        </Button>
                    </div>
                ) : (
                    <div>
                        <Button
                            variant="contained"
                            style={{
                                fontSize: '12px',
                                width: '200px',
                                backgroundColor: '#000F8E',
                                color: 'white',
                                fontFamily: 'Arial',
                                opacity: 0.5
                            }}
                        >
                            Add role
                        </Button>

                        <Button
                            variant="contained"
                            style={{
                                fontSize: '12px',
                                marginLeft:"10px",
                                width: '200px',
                                backgroundColor: '#000F8E',
                                color: 'white',
                                opacity: 0.5
                            }}
                        >
                            Add rights
                        </Button>
                        <Button
                            variant="contained"
                            style={{
                                fontSize: '12px',
                                width: '200px',
                                backgroundColor: '#000F8E',
                                color: 'white',
                                marginLeft:"10px",
                                opacity: 0.5
                            }}
                        >
                            Manage right
                        </Button>
                    </div>
                )
            ),
        }

    ];
    async function rowValue(data) {
        if (!Array.isArray(data)) {
            return [];
        }

        const row = data.map(async (item, index) => {
            const listRight = await handleListRight(item.walletData); // Attendre la résolution de la promesse
            return {
                id: index,
                firstName: item.firstName || null,
                lastName: item.lastName || null,
                email: item.email || null,
                group: (item.groups && item.groups.length > 0) ? item.groups : null,
                phone: (item.attributes && item.attributes.phone && item.attributes.phone.length > 0) ? item.attributes.phone[0] : null,
                address: (item.attributes && item.attributes.adresse && item.attributes.adresse.length > 0) ? item.attributes.adresse[0] : null,
                company: (item.attributes && item.attributes.company && item.attributes.company.length > 0) ? item.attributes.company[0] : null,
                statut: (item.attributes && item.attributes.statut && item.attributes.statut.length > 0) ? item.attributes.statut[0] : null,
                walletEmployee: item.walletData,
                role: item.role ? "FunctionalAdministrator" : "Employee",
                listRight: listRight // Utilisation de la valeur résolue de la promesse
            };
        });

        // Attendre la résolution de toutes les promesses dans le tableau `row`
        return Promise.all(row);
    }


    const handleAddRole = (addressEmployee) => {
        setEmployeeAddress(addressEmployee)
        setAssignDialog(true);
    };
    const handleRightInfos= () => {
        setOpenManageRight(true)
    };
    const handleAddRight= (wallet) => {
        setOpenAddRightDialog(true)
        setWalletAddress(wallet)
    };

    const handleChangeRole = (event) => {
        setRole(event.target.value);
    };

    const handleCloseFormDialog = () => {
        setAssignDialog(false);
        setRole("")
    };
    const handleRole= async () => {
        try{
        //mettre à jour le role de employee
        setAssignDialog(false);
        setOpenLoad(true)
        const provider = new Web3(window.ethereum);
        const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
        const accounts = await provider.eth.getAccounts();
        const updateRoleEmployees= await contract.methods.setIsAdmin(employeeAddress,rolesConfig[role]).send({from: accounts[0]});
            window.location.reload();
    } catch (error) {
            setMessage("your role update has not been taken into account, please try again.")
            setOpenDialog(true)
            setOpenLoad(false)
        console.error(error);
    }
    };

    const handleCloseDialog = () => {
        setOpenDialog(false);
    };
    const handleCloseManageRight = () => {
        setOpenManageRight(false);
    };
    const handleListRight = async (walletAddress) => {
        return filterKeysWithValueTrue(await getRightEmployee(walletAddress))
    };
    async function getRightEmployee(adressWalletEmployee) {
        const provider = new Web3(window.ethereum);
        const contract = new provider.eth.Contract(PermissionManager.abi, ACCOUNT_ADDRESS);
        const employeesOfCompany= await contract.methods.userPermissions(adressWalletEmployee).call({from: address});
        return employeesOfCompany
    }
    const filterKeysWithValueTrue = (obj) => {
        const allowedKeys = ['consume', 'importC', 'present', 'read', 'walletCorpo'];
        if (typeof obj === 'undefined') {
            return "N/A";
        }
        return Object.entries(obj)
            .filter(([key, value]) => allowedKeys.includes(key) && value === true)
            .map(([key, _]) => key)
            .join(", ");
    };


    const handleCloseAddRightDialog = () => {
        setOpenAddRightDialog(false);
        setCheckedValues( resetKeysToFalse(checkedValues))
    };
    const resetKeysToFalse= (object)=> {
        // Parcourir chaque clé de l'objet
        for (const key in object) {
            // Mettre la valeur de chaque clé à false
            object[key] = false;
        }
        return object;
    }
    const  checkIfAnyTrue= (object)=> {
        const values = Object.values(object);
        const hasTrueValue = values.includes(true);
        if (!hasTrueValue) {
            return false;
        }
        return true;
    }
    const handleCheckboxChange = (event) => {
        const { name, checked } = event.target;
        setCheckedValues({ ...checkedValues, [name]: checked });
    };
    const addRight = async () => {
        try {
            if(checkIfAnyTrue(checkedValues)) {
                setAssignDialog(false);
                setOpenLoad(true);
                const provider = new Web3(window.ethereum);
                const contract = new provider.eth.Contract(PermissionManager.abi, ACCOUNT_ADDRESS);
                const employeesOfCompany = await contract.methods.setUserPermission(walletAddress,
                    [checkedValues.Read,checkedValues.Consumption,checkedValues.Presentation,checkedValues.Import,checkedValues.WalletCorporate]).send({from: address});

                if (employeesOfCompany) {
                    setCheckedValues(resetKeysToFalse(checkedValues))
                    setMessage("The rights have been add.");
                    setOpenDialog(true);
                    setOpenLoad(false);
                    setOpenAddRightDialog(false);
                    window.location.reload(true);
                } else {
                    setOpenLoad(false);
                }
            }else {
                setMessage("Please select at least one right to delete");
                setOpenDialog(true);
            }

        }catch (error) {
            setMessage("Error when deleting rights.")
            setOpenDialog(true);
            setOpenLoad(false);
            setCheckedValues( resetKeysToFalse(checkedValues))
            console.log(error)
        }
    };
    return (
        <Box className='corp'>
            {openAddRightDialog && <AddRight
                open={openAddRightDialog}
                handleClose={handleCloseAddRightDialog}
                handleChange={handleCheckboxChange}
                addRight={addRight}
                checkedValues={checkedValues}
            />}

            <Dialog
                open={openManageRight}
                onClose={handleCloseManageRight}
                fullWidth={'xxl'}
                maxWidth={'xxl'}>
                <DialogTitle></DialogTitle>
                <DialogContent>
                    <div>
                        <ManageVcEmployee />
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseManageRight}>Cancel</Button>
                    <Button >Revoked</Button>
                </DialogActions>
            </Dialog>
            <Dialog
                open={openDialog}
                onClose={handleCloseDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                <DialogTitle id="alert-dialog-title">
                    Message
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDialog} autoFocus>
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog
                open={openAssignDialog}
                onClose={handleCloseFormDialog}
            >
                <DialogTitle>Add role</DialogTitle>
                <DialogContent>
            <div>
                <FormControl sx={{ m: 1, width: 500 }}>
                    <InputLabel id="demo-simple-select-label">choosing a role</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={role}
                        onChange={handleChangeRole}>
                        {roleData.map((name) => (
                            <MenuItem key={name} value={name}>
                                {name}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </div>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseFormDialog}>Cancel</Button>
                    <Button onClick={handleRole}>ADD</Button>
                </DialogActions>
            </Dialog>
            <Header/>
            <div className="row company-search container-fluid">
                <div className="col-lg-6 d-flex justify-content-start align-items-center">
                    <h3 style={{ color: 'white', marginLeft:'12px' }}>
                        Administrator page
                    </h3>
                </div>
                <div className='col-lg-6  d-flex justify-content-end align-items-center' style={{ color: 'white', }}>
                    <SearchBar handleValueChange={handleValueChange} />
                </div>
            </div>
            <Backdrop
                sx = {{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open = {openload}>
                <CircularProgress color = "inherit" />
            </Backdrop>
            <div>
                    <div   style={{ marginRight:'2rem', marginTop:'6px', marginLeft:'2rem'}}>
                        <Button variant="contained" style={{ backgroundColor:'#000F8E'}} onClick={() => getVC()}>Home</Button>
                    </div>
                <DataGrid
                    style={{ marginRight:'2rem', marginTop:'20px', marginLeft:'2rem', backgroundColor: 'white', fontSize:'14px' }}
                    rows={rowData}
                    columns={columns}
                    autoHeight
                    initialState={{
                        pagination: {
                            page: 0,
                            pageSize: limit,
                        },
                    }}
                    pageSizeOptions={[0, limit]}
                />
                <Stack spacing={2} sx={{mt:'20px'}}>
                    <Pagination count={sizetab} sx={{marginLeft:'60px',
                        '& .MuiPaginationItem-page.Mui-selected': {
                            color: 'white',
                            backgroundColor:'#000F8E'
                        },
                        '& .MuiPaginationItem-page': {
                            color: '#000F8E',
                        }}} variant="outlined" shape="rounded"  page={currentPage}
                                onChange={handlePageChange} />
                </Stack>
            </div>
        </Box>
    )
}
export default Home;
