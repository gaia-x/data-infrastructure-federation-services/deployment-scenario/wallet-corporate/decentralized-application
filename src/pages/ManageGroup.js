import * as React from 'react';
import axios from "axios";
import '../css/App.css';
import'../css/index.css';
import '../css/Home.css';
import Chip from '@mui/material/Chip';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import {useEffect, useState } from "react";
import DeleteIcon from '@mui/icons-material/Delete'
import {
    Backdrop,
    Box,
    Button,
    Pagination,
    Stack,
    CircularProgress,
    FormControl,
    InputLabel,
    Select,
    OutlinedInput,
    MenuItem,
    Checkbox,
    ListItemText,
    IconButton, Tooltip,

} from "@mui/material";
import { List, ListItem } from '@mui/material';
import Dialog from "@mui/material/Dialog";
import { DataGrid } from '@mui/x-data-grid';
import {useNavigate} from "react-router-dom";
import { getUserInfo } from "../config/keycloak"
import Web3 from "web3";
import CompanyManagement from "../data/abi/CompanyManagement.json";
import { config,rolesConfig } from "../config/config"
import SearchBar from "../component/search";

import {removeLocalStorageItem,getLocalStorageItem,getAllKeys,getKeyByIndex} from "../utils/utils";
import {useAccount} from "wagmi";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContentText from "@mui/material/DialogContentText";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Header from "../component/header";
import {Prism as SyntaxHighlighter} from "react-syntax-highlighter";
import {nightOwl} from "react-syntax-highlighter/dist/cjs/styles/prism";
import  Corporate from '../data/abi/CorporateWallet.json'
import CredentialStorage from '../data/abi/CredentialStorage.json'
import AutocompleteCell from "../component/AutoCompleteCell";
import ManageVcEmployee from "./ManageVcEmployee";
import AddRight from "../component/AddRight";
import PermissionManager from "../data/abi/PermissionManager.json";

function ManageGroup() {
    const [openload, setOpenLoad] = useState(true);
    const [initialLoad, setInitialLoad] = useState(true)
    const [currentPage, setCurrentPage] = useState(1);
    const [sizetab, setSizeTab] = useState(1);
    const navigate = useNavigate();
    const [employeesData, setEmployeesData] = useState([]);
    const ACCOUNT_ADDRESS =config.address;
    const { address, isConnecting, isDisconnected } = useAccount();
    const [token, setToken] = useState(getLocalStorageItem('token'));
    const [idwallet, setIdWallet] = useState(getLocalStorageItem('walletId'));
    const [roleData, setRoleData] = useState([]);
    const [roleEmployee, setRoleEmployee] = useState('');
    const [openDialog, setOpenDialog] = useState(false);
    const [message, setMessage] = useState("");
    const [selectedRows, setSelectedRows] = useState([]);

    const [showvc, setShowVc] = useState(false);
    const [valuevc, setValueVC] = useState({});

    const [listOfVC, setListOfVC] = useState([]);
    const [inputValue, setInputValue] = useState('');
    const [selectedValues, setSelectedValues] = useState([]);
    const [listOfGroup, setListofGroup] = useState([]);

    const [personNameGroup, setPersonNameGroup] = useState([]);
    const [employeesDataGroup, setEmployeesDataGroup] = useState([]);
    const [employeesDataGroupAddress, setEmployeesDataGroupAddress] = useState([]);

    const [openFormDialogGroup, setOpenFormDialogGroup] = React.useState(false);
    const [openAssignDialogGroup, setOpenAssignDialogGroup] = React.useState(false);
    const [openAssignListGroup, setOpenAssignListGroup] = React.useState(false);
    const [openManageRight, setOpenManageRight] =useState(false);

    const [nameGroupManage, setNameGroupManage] = useState("");
    const [nameGroup, setNameGroup] = useState("");
    const [rowsGroup, setRowsGroup] = useState([]);
    const [rows, setRows] = useState([]);

    const [selectedGroupName, setSelectedGroupName] = useState('');
    const [openAddRightDialog, setOpenAddRightDialog] = useState(false);
    const [checkedValues, setCheckedValues] = useState({
        Read: false,
        Consumption: false,
        Presentation: false,
        Import: false,
        WalletCorporate: false,
    });


    const  checkIfAnyTrue= (object)=> {
        const values = Object.values(object);
        const hasTrueValue = values.includes(true);
        if (!hasTrueValue) {
            return false;
        }
        return true;
    }

    const handleCheckboxChange = (event) => {
        const { name, checked } = event.target;
        setCheckedValues(prev => ({ ...prev, [name]: checked }));
    };

    const handleOpenAddRightDialog = () => {
        setOpenAddRightDialog(true);
    };

    const handleCloseAddRightDialog = () => {
        setOpenAddRightDialog(false);
        setCheckedValues({
            Read: false,
            Consumption: false,
            Presentation: false,
            Import: false,
            WalletCorporate: false,
        });
    };

    const handleAddRight= (group) => {
        setOpenAddRightDialog(true)
        setSelectedGroupName(group)
    };

    useEffect(() => {
        if (initialLoad) {
            setInitialLoad(false);
            return;
        }
        setOpenLoad(true);
        if(isDisconnected){
            logOut()
        }else {
            handleIsHaveRole()
            setRoleData(getAllKeys(rolesConfig))
            handleGetGroup();
            if(token && idwallet){
                getVCWallet(token,idwallet)
                getAccountWallet(token);
            }else{
                navigate('/')
            }
        }
        document.title = "Manage group";
    }, [initialLoad,address,isDisconnected]);
    const handleValueChange= (event) => {

    };

    const isStorageVcTIpfs = async (Vcs) => {
        const vcNotShare = [];
        try {
            // Fonction pour gérer le partage de chaque VC et la mise à jour des tableaux
            if (typeof window.ethereum !== "undefined") {
                const provider = new Web3(window.ethereum);
                const contract = new provider.eth.Contract(Corporate.abi, ACCOUNT_ADDRESS);
                const handleShareAndPush = async (vc) => {
                    try {
                            const isExistVc = await contract.methods.isExist(vc.parsedDocument.id).call()
                            if (!isExistVc) {
                                vcNotShare.push(vc); // Ajouter l'identifiant au tableau idArray
                        }
                    } catch (error) {
                        console.error("An error occurred while sharing VC:", error);
                    }
                };

                // Parcourir chaque objet VC dans le tableau Vcs et partager
                for (const vc of Vcs) {
                    await handleShareAndPush(vc); // Attendre que le partage soit terminé avant de passer au suivant
                }
            }
            setListOfVC(vcNotShare);
            console.log("urlArray:", vcNotShare);
        }catch (error) {
            console.error("An error occurred while sharing VC:", error);
        }
    }
    const getVCWallet = async (tokenData,accountId) => {

        try {
            if (!tokenData) {
                navigate('/');
                return;
            }
            const response = await axios.get('http://localhost:7001/wallet-api/wallet/'+accountId+'/credentials', {
                headers: {
                    Authorization: `Bearer ${tokenData}`
                }
            });
            console.log("response.data")
            console.log(response.data)
           await isStorageVcTIpfs(response.data)

        } catch (error) {
            console.error('Erreur lors de la récupération des données:', error);
            setOpenLoad(false);
        }
    };

    const getRole=(isAdmin)=> {
        switch (isAdmin) {
            case false:
                navigate('/');
        }
    }
    const handleIsHaveRole = async () => {
        const provider = new Web3(window.ethereum);
        const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
        const employeesOfCompany= await contract.methods.employees(address).call();
        getRole(employeesOfCompany['isAdmin'])
    }

    const logOut= async () => {

        try {
            removeLocalStorageItem("token");
            removeLocalStorageItem("walletId");
            navigate('/');
        } catch (error) {
            console.error('Erreur lors de la déconnexion:', error);
        }
    };

    async function filterEmployeesData(usersInfo,accountId) {
        const provider = new Web3(window.ethereum);
        const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
        const accounts = await provider.eth.getAccounts();
        const employeesOfCompany= await contract.methods.getEmployeesOfMyCompany().call({from: address});
        const emailPromises = usersInfo.map(async item => {
            if (item.hasOwnProperty("email")) {
                const index = employeesOfCompany['1'].findIndex(employee => employee.email === item.email);
                console.log('index')
                console.log(index)
                if (index !== -1) {
                    return { ...item, walletData: employeesOfCompany['0'][index], role: employeesOfCompany['1'][index]['role'] };
                }
            } else {
                return null;
            }
        });

        const emailResults = await Promise.all(emailPromises);

        return emailResults.filter(Boolean); // Filtrer les résultats pour supprimer les valeurs null
    }


    const getVC = () => {
        navigate('/allvc');
    };

    const handlePageChange = (event, page) => {
        setCurrentPage(page);
    };

    const handleGetGroup = async () => {

                try {
                    // Fonction pour gérer le partage de chaque VC et la mise à jour des tableaux
                    if (typeof window.ethereum !== "undefined") {
                        const provider = new Web3(window.ethereum);
                        const contract = new provider.eth.Contract(CredentialStorage.abi, ACCOUNT_ADDRESS);
                        const accounts = await provider.eth.getAccounts();
                        const allGroup = await contract.methods.getAllGroups().call({from: accounts[0]});
                        setListofGroup(allGroup);
                        const rowsData = rowValue(allGroup);
                        setRows(rowsData)
                        enrichGroupsWithMemberCount(rowsData);
                    }
                } catch (error) {
                    setOpenLoad(false)
                    console.error("An error occurred while sharing VC:", error);
                }
    };

    const handleCloseDialog = () => {
        setOpenDialog(false);
    };

    const handleSelectionChange = (selection) => {
        setSelectedRows(selection)
    };

    const handleClosec = () => {
        setShowVc(false);
    };

    const listExistGroup = [
    ];

    const handleGroupChange= (event, newInputValue) => {
        setInputValue(newInputValue);
    };

    const handleAddValue = (event, newValue) => {
        setSelectedValues([...selectedValues, newValue]);
        setInputValue(''); // Clear input after adding
    };
    const handleCloseFormDialogGroup = () => {
        setOpenFormDialogGroup(false);
        setSelectedValues([])
    };
    const handleAddGroup = async () => {
        setOpenFormDialogGroup(false)
        if (selectedValues.length === 0) {
            setMessage("Enter the group name")
            setOpenDialog(true);
        } else {
            try {
                setOpenLoad(true);
                // Fonction pour gérer le partage de chaque VC et la mise à jour des tableaux
                if (typeof window.ethereum !== "undefined") {
                    const provider = new Web3(window.ethereum);
                    const contract = new provider.eth.Contract(CredentialStorage.abi, ACCOUNT_ADDRESS);
                    const accounts = await provider.eth.getAccounts();
                    const result = await contract.methods.createGroups(selectedValues).send({from: accounts[0]});
                    if(result){
                        window.location.reload(true);
                    }
                }
            } catch (error) {
                setMessage("Error to add group")
                setOpenDialog(true)
                setOpenLoad(false)
            }
        }
    }

    const handleRemoveGroup= async (nameGroup) => {
        setOpenLoad(true);
        try {
            // Fonction pour gérer le partage de chaque VC et la mise à jour des tableaux
            if (typeof window.ethereum !== "undefined") {
                const provider = new Web3(window.ethereum);
                const contract = new provider.eth.Contract(CredentialStorage.abi, ACCOUNT_ADDRESS);
                const accounts = await provider.eth.getAccounts();
                const result = await contract.methods.deleteGroup(nameGroup).send({from: accounts[0]});
                if(result){
                    window.location.reload(true);
                }
            }
        } catch (error) {
            setMessage("error when deleting group")
            setOpenDialog(true)
            setOpenLoad(false)
        }
    };
    const handleOpenFormDialogGroup = () => {
        setOpenFormDialogGroup(true);
    };

    const handleCloseAssignDialogGroup = () => {
        setOpenAssignDialogGroup(false)
    };
    const handleCloseListGroup = () => {
        setOpenAssignListGroup(false)
    };
    const handleAddEmployeeGroup = async () => {
        let adressEmployee = [];

        if (personNameGroup.length> 0 && employeesDataGroupAddress.length>0) {
        for (let i = 0; i < personNameGroup.length; i++) {
            adressEmployee.push(employeesDataGroupAddress[i+1]);
        }
            try {
                setOpenAssignDialogGroup(false)
               setOpenLoad(true)
                // Fonction pour gérer le partage de chaque VC et la mise à jour des tableaux
                if (typeof window.ethereum !== "undefined") {
                    const provider = new Web3(window.ethereum);
                    const contract = new provider.eth.Contract(CredentialStorage.abi, ACCOUNT_ADDRESS);
                    const accounts = await provider.eth.getAccounts();
                    const resultAddEmployee= await contract.methods.addMembersToGroup(nameGroup,adressEmployee).send({from: address});
                    if(resultAddEmployee){
                        window.location.reload(true);
                    }
                }
            } catch (error) {
                setOpenAssignDialogGroup(false)
                setMessage("Error when adding an employee to a group.")
                setOpenDialog(true)
                setOpenLoad(false)
            }
        }

    };

    const handleGetMembersToGroup = async (name) => {
        try {
            // Vérifie si MetaMask est disponible dans le navigateur
            if (typeof window.ethereum !== "undefined") {
                // Initialise le fournisseur Web3 avec MetaMask
                const provider = new Web3(window.ethereum);
                // Initialise le contrat avec l'ABI et l'adresse du contrat
                const contract = new provider.eth.Contract(CredentialStorage.abi, ACCOUNT_ADDRESS);
                // Appelle la fonction getMembersOfGroup du contrat avec le nom du groupe
                const membersOfGroup = await contract.methods.getMembersOfGroup(name).call({ from: address });
                // Retourne la longueur du tableau des membres
                return membersOfGroup;
            }
        } catch (error) {
            // En cas d'erreur, affiche l'erreur dans la console et retourne []
            console.log(error);
            return [];
        }
    };

// Fonction pour enrichir chaque groupe avec le nombre de membres
    const enrichGroupsWithMemberCount = async (groups) => {
        const enrichedGroups = [];
        // Parcours de chaque groupe dans le tableau
        for (const group of groups) {
            const memberCount = await handleGetMembersToGroup(group.name_group);
            // Crée une copie du groupe avec le champ `nombre` ajouté
            const enrichedGroup = { ...group, number_employee: memberCount.length };
            enrichedGroups.push(enrichedGroup);
        }
       setRowsGroup(enrichedGroups)
    };

    const columns = [
        { field: 'id', headerName: 'ID', width: 120 , align: 'center',},
        { field: 'name_group', headerName: 'Group', width: 250, align: 'center', },
        { field: 'number_employee', headerName: 'Members', width: 170 , align: 'center',},
        {
            field: 'employees',
            headerName: 'Employees', headerClassName: 'column-text',
            description: 'Gestion des employers',
            sortable: false,
            flex: 1, headerAlign: 'center', align: 'center',
            renderCell: (params) => (
                <div style={{ display: 'flex', alignItems: 'center', gap: '10px', width: '95%' }}>
                    <AutocompleteCell
                        id="example-autocomplete"
                        listEmployees={employeesData}
                        nameGroup={params.row.name_group}
                        handleLoad={handleLoad}
                    /></div>
            )
        },
        {
            field: 'action',
            headerName: 'Action', headerClassName: 'column-text',
            description: 'Gestion des action sur les groupes',
            sortable: false,
            width: 400, headerAlign: 'center', align: 'center',
            renderCell: (params) => (
                <div style={{ display: 'flex', gap: '10px',margin:'10px'}}>
                    <Button
                        variant="contained"
                        style={{
                            fontSize: '12px',
                            marginLeft:"10px",
                            width: '200px',
                            backgroundColor: '#000F8E',
                            color: 'white'
                        }}
                        onClick={() => handleAddRight(params.row.name_group)}>
                        Add rights
                    </Button>

                    <Tooltip title="Delete Group">
                    <IconButton aria-label="delete" size="medium"  onClick={() => handleRemoveGroup(params.row.name_group)}  style={{
                        backgroundColor: 'red',
                        color: 'white',
                    }}>
                        <DeleteIcon fontSize="inherit"  />
                    </IconButton>
                    </Tooltip>
                </div>
            ),
        }
    ];
    function rowValue (data) {
        if (!Array.isArray(data)) {
            return [];
        }
        const row = data.map((item, index) => {
            return {
                id: index+1,
                name_group: item
            }
        });
        return row;
    }

    const handleChangeGroup = (event) => {
        const {
            target: { value },
        } = event;
        setPersonNameGroup(
            typeof value === 'string' ? value.split(',') : value,
        );
    };

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 250,
            },
        },
    };

    const getAccountWallet = async (tokenData) => {
        try {
            if (!tokenData) {
                navigate('/');
                return;
            }
            const usersInfo=await getUserInfo();
            filterEmployeesData(usersInfo,idwallet)
                .then(filteredEmployeesData => {
                    const filteredData = filteredEmployeesData.filter(item => item.walletData);
                    setEmployeesData(filteredData);
                }).catch(error => {
                console.error("Une erreur s'est produite :", error);
            });
            setOpenLoad(false);
        } catch (error) {
            console.error('Erreur lors de la récupération des données:', error);
            setOpenLoad(false);
        }
    };
    const handleCloseManageRight = () => {
        setOpenManageRight(false);
    };
    const handleRightInfos= (name) => {
        setOpenManageRight(true);
        setNameGroupManage(name)
    };
    const handleLoad = (isLoad) => {
        setOpenLoad(isLoad)
    }
    const getRowClassName = (params) => {
        return "customRowDataGrid"; // Nom de la classe pour les lignes personnalisées
    };

    const resetKeysToFalse= (object)=> {
        for (const key in object) {
            object[key] = false;
        }
        return object;
    }

    const addRight = async () => {
        try {
            if(checkIfAnyTrue(checkedValues)) {
                setOpenLoad(true);
                const provider = new Web3(window.ethereum);
                const contract = new provider.eth.Contract(PermissionManager.abi, ACCOUNT_ADDRESS);
                const employeesOfCompany = await contract.methods.setGroupPermission(selectedGroupName,
                    [checkedValues.Read,checkedValues.Consumption,checkedValues.Presentation,checkedValues.Import,checkedValues.WalletCorporate]).send({from: address});

                if (employeesOfCompany) {
                    setCheckedValues(resetKeysToFalse(checkedValues))
                    setMessage("The rights have been add.");
                    setOpenDialog(true);
                    setOpenLoad(false);
                    setOpenAddRightDialog(false);
                    window.location.reload(true);
                } else {
                    setOpenLoad(false);
                }
            }else {
                setMessage("Please select at least one right to delete");
                setOpenDialog(true);
            }

        }catch (error) {
            setMessage("Error when deleting rights.")
            setOpenDialog(true);
            setOpenLoad(false);
            setCheckedValues( resetKeysToFalse(checkedValues))
            console.log(error)
        }
    };

    return (
        <Box className='corp'>
            <AddRight
                open={openAddRightDialog}
                handleClose={handleCloseAddRightDialog}
                handleChange={handleCheckboxChange}
                addRight={addRight}
                checkedValues={checkedValues}
            />
            <Dialog
                open={openManageRight}
                onClose={handleCloseManageRight}
                fullWidth={'xxl'}
                maxWidth={'xxl'}>
                <DialogTitle></DialogTitle>
                <DialogContent>
                    <div>
                        <ManageVcEmployee nameOfGroupToManage={nameGroupManage}/>
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseManageRight}>Cancel</Button>
                    <Button >Revoked</Button>
                </DialogActions>
            </Dialog>
            <Dialog
                open={openAssignDialogGroup}
                onClose={handleCloseFormDialogGroup}>
                <DialogTitle>Add Employees</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Please choose employee.
                    </DialogContentText>
                    <div>
                        <FormControl sx={{ m: 1, width: 500 }}>
                            <InputLabel id="demo-multiple-checkbox-label">Email</InputLabel>
                            <Select
                                labelId="demo-multiple-checkbox-label"
                                id="demo-multiple-checkbox"
                                multiple
                                value={personNameGroup}
                                onChange={handleChangeGroup}
                                input={<OutlinedInput label="Tag" />}
                                renderValue={(selected) => selected.join(', ')}
                                MenuProps={MenuProps}>
                                {employeesDataGroup.map((name) => (
                                    name.email !== "" && ( // Vérifiez si l'e-mail n'est pas vide
                                        <MenuItem key={name.email} value={name.email}>
                                            <Checkbox checked={personNameGroup.indexOf(name.email) > -1} />
                                            <ListItemText primary={name.email} />
                                        </MenuItem>
                                    )
                                ))}
                            </Select>
                        </FormControl>
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseAssignDialogGroup}>Cancel</Button>
                    <Button onClick={handleAddEmployeeGroup}>ADD</Button>
                </DialogActions>
            </Dialog>

 <Dialog
                open={openAssignListGroup}
                onClose={handleCloseListGroup}>
                <DialogTitle>List of wallet employees</DialogTitle>
                <DialogContent>
                    <div>
                        <List
                            sx={{
                                width: '100%',
                                maxWidth: 500,
                                bgcolor: 'background.paper',
                                position: 'relative',
                                overflow: 'auto',
                                maxHeight: 300,
                                '& ul': { padding: 0 },
                            }}
                            subheader={<li/>}>
                            {listOfGroup.map((sectionId,index) => (
                                <li key={`section-${sectionId}`}>
                                    <ul>
                                            <ListItem key={sectionId}>
                                                <ListItemText primary={`${index+1})  ${sectionId}`} />
                                            </ListItem>
                                    </ul>
                                </li>
                            ))}
                        </List>
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseListGroup}>Cancel</Button>
                </DialogActions>
            </Dialog>
            <Dialog
                open={openFormDialogGroup}
                onClose={handleCloseFormDialogGroup}>
                <DialogTitle>Add group</DialogTitle>
                <DialogContent>
                    <Autocomplete
                        multiple
                        id="tags-filled"
                        sx={{ width: 400,marginTop:'10px' }}
                        options={listExistGroup.map((option) => option)}
                        freeSolo
                        value={selectedValues}
                        onChange={(event, newValue) => setSelectedValues(newValue)}
                        inputValue={inputValue}
                        onInputChange={handleGroupChange}
                        renderTags={(value, getTagProps) =>
                            value.map((option, index) => (
                                <Chip key={index} variant="outlined" label={option} {...getTagProps({ index })}
                                      sx={{  backgroundColor: '#000F8E !important' ,
                                    color: 'white' }} />
                            ))
                        }
                        renderInput={(params) => (
                            <TextField
                                {...params}
                                label="list of groups you wish to add"
                                placeholder="write group name"
                                InputLabelProps={{ shrink: true }}
                            />
                        )}
                        onKeyDown={(event) => {
                            if (event.key === 'Enter') {
                                handleAddValue(null, inputValue);
                            }
                        }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseFormDialogGroup}>Cancel</Button>
                    <Button onClick={handleAddGroup}>ADD</Button>
                </DialogActions>
            </Dialog>
            {showvc && (
                <div >
                    <Dialog open = {showvc} onClose = {handleClosec} sx = {{ color: '#fff', fontSize:"17px", zIndex: (theme) => theme.zIndex.drawer + 1 }} >
                        <DialogTitle style = {{color:'white', background: 'linear-gradient(270deg, #000F8E 25%, #A931F6 129.76%)'}}>Content</DialogTitle>
                        <SyntaxHighlighter language="json" style={nightOwl}  customStyle={{ width: '100%' }}>
                            {JSON.stringify(valuevc, null, 2)}
                        </SyntaxHighlighter>
                    </Dialog>
                </div>
            )}
            <Dialog
                open={openDialog}
                onClose={handleCloseDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Message
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDialog} autoFocus>
                        OK
                    </Button>
                </DialogActions>
            </Dialog>
            <Header/>
            <div className="row company-search container-fluid">
                <div className="col-lg-6 d-flex justify-content-start align-items-center">
                    <h3 style={{ color: 'white', marginLeft:'12px' }}>
                        List of groups
                    </h3>
                </div>
                <div className='col-lg-6  d-flex justify-content-end align-items-center' style={{ color: 'white', }}>
                    <SearchBar handleValueChange={handleValueChange} />
                </div>
            </div>
            <Backdrop
                sx = {{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open = {openload}>
                <CircularProgress color = "inherit" />
            </Backdrop>
            <div>
                <div   style={{ marginRight:'2rem', marginTop:'6px', marginLeft:'2rem'}}>
                    <Button variant="contained" style={{ backgroundColor:'#000F8E'}} onClick={() => getVC()}>Home</Button>
                </div>
                <div className='m-3'>
               <Button variant="contained"  style={{ backgroundColor:'#000F8E'}}  onClick={handleOpenFormDialogGroup}>Add groups</Button>
                </div>
                <div style={{ margin:'40px'}}>
                <DataGrid
                    rows={rowsGroup}
                    columns={columns}
                    initialState={{
                        pagination: {
                            paginationModel: { page: 0, pageSize: 100 },
                        },
                    }}
                    sx = {{ fontSize: "16px",fontFamily: 'Titillium Web, sans-serif !important'}}
                    pageSizeOptions={[0, 100]}
                    onRowSelectionModelChange={handleSelectionChange}
                    className="customRowDataGrid"
                    getCellClassName={getRowClassName}
                />
                </div>
                <Stack spacing={2} sx={{mt:'20px'}}>
                    <Pagination count={sizetab} sx={{marginLeft:'60px',
                        '& .MuiPaginationItem-page.Mui-selected': {
                            color: 'white',
                            backgroundColor:'#000F8E'
                        },
                        '& .MuiPaginationItem-page': {
                            color: '#000F8E',
                        }}} variant="outlined" shape="rounded"  page={currentPage}
                                onChange={handlePageChange} />
                </Stack>
            </div>
        </Box>
    )
}


export default ManageGroup;
