import * as React from 'react';
import {useEffect, useState} from 'react';
import '../css/App.css';
import '../css/index.css';
import '../css/Home.css';
import Web3 from 'web3';
import axios from "axios";
import {getUserInfo} from "../config/keycloak"
import CarteVC from '../component/CarteVc';
import Header from "../component/header";
import {Backdrop, Box, Button, CircularProgress, Pagination, Stack} from "@mui/material";
import {useNavigate} from "react-router-dom";
import {getFirstNElements, getLocalStorageItem, removeLocalStorageItem, roundDecimalToNextInteger} from "../utils/utils"
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import {Prism as SyntaxHighlighter} from 'react-syntax-highlighter';
import {nightOwl} from 'react-syntax-highlighter/dist/esm/styles/prism';
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import DialogContentText from "@mui/material/DialogContentText";
import {useAccount} from "wagmi";
import Corporate from '../data/abi/CorporateWallet.json'
import CompanyManagement from '../data/abi/CompanyManagement.json'
import {config} from "../config/config"
import SearchBar from "../component/search";
import DialogAuthorizedComponent from "../component/dialog/Authorized";

function ShowVC() {

    const ACCOUNT_ADDRESS = config.address;
    const LIMIT = 8;
    const [openload, setOpenLoad] = useState(true);
    const [limit, setLimit] = useState(10);
    const [initialLoad, setInitialLoad] = useState(true);
    const [currentPage, setCurrentPage] = useState(1);
    const [sizetab, setSizeTab] = useState(8);
    const [walletsData, setWalletsData] = useState('');
    const [listOfVC, setListOfVC] = useState([]);
    const navigate = useNavigate();
    const [showvc, setShowVc] = useState(false);
    const [valuevc, setValueVC] = useState({});
    const [openFormDialog, setOpenFormDialog] = React.useState(false);
    const [openAssignDialog, setAssignDialog] = React.useState(false);
    const [groupName, setGroupName] = useState('');
    const [userAssign, setUserAssign] = useState([]);
    const [vcId, setVcId] = useState("");
    const [openDialog, setOpenDialog] = useState(false);
    const [message, setMessage] = useState("");
    const [error, setError] = useState('');
    const [personName, setPersonName] = useState([]);
    const [employeesData, setEmployeesData] = useState([]);
    const {address, isConnecting, isDisconnected} = useAccount();
    const [token, setToken] = useState(getLocalStorageItem('token'));
    const [idwallet, setIdWallet] = useState(getLocalStorageItem('walletId'));
    const [listOfGroup, setListofGroup] = useState([]);
    const [groupSelection, setGroupSelection] = useState([]);

    const handleGroupChange = (event) => {
        const {
            target: { value },
        } = event;
        setGroupSelection(
            // Pour les sélections multiples, on peut avoir besoin de diviser les valeurs
            typeof value === 'string' ? value.split(',') : value,
        );
    };
    const getRole = (isAdmin) => {
        switch (isAdmin) {
            case false:
                navigate('/');
        }
    }
    const handleIsHaveRole = async () => {
        const provider = new Web3(window.ethereum);
        const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
        const employeesOfCompany = await contract.methods.employees(address).call();
        getRole(employeesOfCompany['isAdmin'])
    }
    const handleValueChange = (event) => {
    };
    const handleAssignVC = (vc) => {
        if (employeesData.length !== 0) {
            setVcId(vc.id);
            setAssignDialog(true);
        } else {
            setMessage("You cannot assign a vc because no employer has yet added a wallet.")
            setOpenDialog(true);
        }

    };

    useEffect(() => {
        if (initialLoad) {
            setInitialLoad(false);
            return;
        }

        if (isDisconnected) {
            logOut()
        } else {
            setOpenLoad(true);
            handleIsHaveRole()
            getAccountWallet(token, idwallet);
            showVcWallet();
            handleGetGroup();
        }
        document.title = "Show VCs";

    }, [initialLoad, address, isDisconnected]);
    const logOut = async () => {

        try {
            removeLocalStorageItem("token");
            removeLocalStorageItem("walletId");
            navigate('/');
        } catch (error) {
            console.error('Erreur lors de la déconnexion:', error);
        }
    };

    const getAccountWallet = async (tokenData, walletId) => {
        try {
            if (!tokenData && !walletId) {
                navigate('/');
                return;
            }
            setWalletsData(walletId);
            getVC(tokenData, walletId);
            setOpenLoad(false);
        } catch (error) {
            console.error('Erreur lors de la récupération des données:', error);
            setOpenLoad(false);
        }
    };

    const getVC = async (tokenData, accountId) => {

        try {
            setOpenLoad(true);
            if (!tokenData) {
                navigate('/');
                return;
            }
            const response = await axios.get('http://localhost:7001/wallet-api/wallet/' + accountId + '/credentials', {
                headers: {
                    Authorization: `Bearer ${tokenData}`
                }
            });
            await isStorageVcTIpfs(response.data);
            console.log(response.data)
            setSizeTab(roundDecimalToNextInteger(response.data.length / LIMIT));
            const usersInfo = await getUserInfo();

            filterEmployeesData(usersInfo, accountId)
                .then(filteredEmployeesData => {
                    setEmployeesData(filteredEmployeesData);
                }).catch(error => {
                console.error("Une erreur s'est produite :", error);
            });
            setOpenLoad(false);
        } catch (error) {
            console.error('Erreur lors de la récupération des données:', error);
            setOpenLoad(false);
        }
    };
    const handleChange = (event) => {
        const {
            target: {value},
        } = event;
        setPersonName(
            // On autofill we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
        );
    };
    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 250,
            },
        },
    };
    const handleCloseFormDialog = () => {
        setOpenFormDialog(false);
        setGroupName('');
        setError('');
    };

    const handleSearch = () => {
        // Function to search user
    };
    const getEmployee = () => {
        navigate("/home")
    };
    const showIpfsVC = () => {
        navigate("/manage_vc")
    };

    const showVcWallet = async () => {
        const login = await axios.post('http://localhost:7001/wallet-api/auth/login', {
            "type": "email",
            "email": config.email,
            "password": config.password
        });

        const idVc = await axios.get('http://localhost:7001/wallet-api/wallet/accounts/wallets', {
            headers: {
                Authorization: `Bearer ${login.data.token}`
            }
        });
        setVcId(idVc.data.wallets[0].id)
    };

    async function filterEmployeesData(usersInfo, accountId) {
        const provider = new Web3(window.ethereum);
        const contract = new provider.eth.Contract(CompanyManagement.abi, ACCOUNT_ADDRESS);
        const employeesOfCompany = await contract.methods.getEmployeesOfMyCompany().call({from: address});
        const emailPromises = usersInfo.map(async item => {
            if (item.hasOwnProperty("email")) {
                const index = employeesOfCompany['1'].findIndex(employee => employee.email === item.email);
                if (index !== -1) {
                    return {
                        ...item,
                        walletData: employeesOfCompany['0'][index],
                        role: employeesOfCompany['1'][index]['role']
                    };
                }
            } else {
                return null;
            }
        });

        const emailResults = await Promise.all(emailPromises);
        return emailResults.filter(Boolean); // Filtrer les résultats pour supprimer les valeurs null
    }

    const handleGetGroup = async () => {
        try {
            if (typeof window.ethereum !== "undefined") {
                const provider = new Web3(window.ethereum);
                const contract = new provider.eth.Contract(Corporate.abi, ACCOUNT_ADDRESS);
                const accounts = await provider.eth.getAccounts();
                const allGroup = await contract.methods.getAllGroups().call({from: accounts[0]});
                setListofGroup(allGroup);
            }
        } catch (error) {
            setOpenLoad(false)
            console.error("An error occurred while getting groups: ", error);
        }
    };
    const handlePageChange = (event, page) => {
        setCurrentPage(page);
    };
    const isStorageVcTIpfs = async (Vcs) => {
        const isVcShare = [];
        try {
            // Fonction pour gérer le partage de chaque VC et la mise à jour des tableaux
            if (typeof window.ethereum !== "undefined") {
                const provider = new Web3(window.ethereum);
                const contract = new provider.eth.Contract(Corporate.abi, ACCOUNT_ADDRESS);

                const handleShareAndPush = async (vc) => {
                    try {
                        const isExistVc = await contract.methods.isExist(vc.parsedDocument.id).call()
                        if (!isExistVc) {
                            isVcShare.push({isShare: false, data: vc}); // Ajouter l'identifiant au tableau idArray
                        } else {
                            isVcShare.push({isShare: true, data: vc});
                        }
                    } catch (error) {
                        console.error("An error occurred while sharing VC:", error);
                    }
                };
                for (const vc of Vcs) {
                    await handleShareAndPush(vc); // Attendre que le partage soit terminé avant de passer au suivant
                }
            }
            setListOfVC(isVcShare);
        } catch (error) {
            console.error("An error occurred while sharing VC:", error);
        }
    }

    const handleClosec = () => {
        setShowVc(false);
    };
    const showGroup = () => {
        navigate('/manage_group')
    };

    const handleShowVC = (vcValue) => {
        if (vcValue) {
            setShowVc(true);
            setValueVC(vcValue)
        } else {
            console.log('absence of vc.');
        }

    };

    const handleAddVC = async () => {
        try {
            if (typeof window.ethereum !== "undefined") {
                const provider = new Web3(window.ethereum);
                const contract = new provider.eth.Contract(Corporate.abi, ACCOUNT_ADDRESS);
                if (personName.length != 0) {

                    const emailSet = new Set(personName);
                    await contract.methods.addAuthorizedUsers(vcId,
                        employeesData.filter(
                            employee => emailSet.has(
                                employee.email))
                            .map(employee => employee.walletData))
                        .send({from: address})
                }
                console.log(groupSelection);
                if(groupSelection.length > 0) {
                    await contract.methods.addAuthorizedGroups(vcId,
                        groupSelection)
                        .send({from: address})
                }
            }
        } catch (error) {
            setAssignDialog(false)
            setMessage("An error occurred when you add the VC.")
            setOpenDialog(true);
            console.error(error);
        }
    };
    const handleCloseAssignDialog = () => {
        setAssignDialog(false);
        setPersonName([])
        setUserAssign({})
    };

    const handleCloseDialog = () => {
        setOpenDialog(false);
    };

    return (
        <Box className='corp'>

            <Dialog
                open={openDialog}
                onClose={handleCloseDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    Message
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDialog} autoFocus>
                        OK
                    </Button>
                </DialogActions>
            </Dialog>

            <DialogAuthorizedComponent
                open={openAssignDialog}
                handleClose={handleCloseAssignDialog}
                employeesData={employeesData}
                personName={personName}
                listOfGroup={listOfGroup}
                groupSelection={groupSelection}
                handleChange={handleChange}
                handleGroupChange={handleGroupChange}
                handleAddVC={handleAddVC}
            />

            {showvc && (
                <div>
                    <Dialog open={showvc} onClose={handleClosec}
                            sx={{color: '#fff', fontSize: "17px", zIndex: (theme) => theme.zIndex.drawer + 1}}>
                        <DialogTitle style={{
                            color: 'white',
                            background: 'linear-gradient(270deg, #000F8E 25%, #A931F6 129.76%)'
                        }}>Content</DialogTitle>
                        <SyntaxHighlighter language="json" style={nightOwl} customStyle={{width: '100%'}}>
                            {JSON.stringify(valuevc, null, 2)}
                        </SyntaxHighlighter>
                    </Dialog>
                </div>
            )}
            <Header/>
            <div className="row company-search m-0">
                <div className="col-lg-6 d-flex justify-content-start align-items-center">
                    <h3 style={{color: 'white', marginLeft: '12px'}}>
                        Welcome to the corporate wallet
                    </h3>
                </div>
                <div className='col-lg-6  d-flex justify-content-end align-items-center' style={{color: 'white',}}>
                    <SearchBar handleValueChange={handleValueChange}/>
                </div>
            </div>

            <div className='mt-3'>
                <span className='ms-4'> <Button variant="contained" style={{backgroundColor: "#000F8E"}}
                                                onClick={getEmployee}>Show all employee</Button></span>
                <span className='ms-4'> <Button variant="contained" style={{backgroundColor: "#000F8E"}}
                                                onClick={showIpfsVC}>Manage Vc</Button></span>
                <span className='ms-4'> <Button variant="contained" style={{backgroundColor: "#000F8E"}}
                                                onClick={showGroup}>Group administration</Button></span>
                <div className="container">
                    <div className="row">
                        {getFirstNElements(listOfVC, currentPage).map((vc, index) => (
                            <CarteVC vc={vc} index={index}
                                     handleShowVC={handleShowVC} handleAssignVC={handleAssignVC}/>))
                        }
                    </div>
                </div>

            </div>
            <Backdrop
                sx={{color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1}}
                open={openload}>
                <CircularProgress color="inherit"/>
            </Backdrop>

            <div className='row m-0  container-fluid d-flex  align-items-center '>
                <Stack spacing={2} sx={{mt: '20px', ml: '20px', mb: '20px'}}
                       className='col justify-content-end align-items-center'>
                    <Pagination sx={{
                        marginLeft: '60px',
                        '& .MuiPaginationItem-page.Mui-selected': {
                            color: 'white',
                            backgroundColor: '#000F8E'
                        },
                        '& .MuiPaginationItem-page': {
                            color: '#000F8E',
                        }
                    }} variant="outlined" shape="rounded" page={currentPage} count={sizetab}
                                onChange={handlePageChange}/>
                </Stack>
            </div>
        </Box>
    )
}

export default ShowVC;
