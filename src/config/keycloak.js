const getKeycloakClient = async () => {
    const keycloakAdmin = (await import('@keycloak/keycloak-admin-client')).default;
    const client = new keycloakAdmin({
        baseUrl: "https://keycloak.demo23.gxfs.fr",
        realmName: 'DappsCorporate',
    });

    await client.auth({
        username: "admin",
        password: "test",
        clientId: 'dapps-login',
        grantType: 'password'
    });
    return client;
}

const getUserInfo = async () => {
    try {
        const keycloakClient = await getKeycloakClient();
        const users = await keycloakClient.users.find();
        for (const user of users) {
            const groups = await keycloakClient.users.listGroups({ id: user.id });
            user.groups = groups.map(group => group.name);
        }
        console.log(users);
     return users;
    } catch (error) {
        console.log(error)
       throw new Error(error);
    }
}

module.exports = {
    getUserInfo
}