import Keycloak from 'keycloak-js';

const keycloakConfig = new Keycloak({
    realm: 'DappsCorporate',
    url: 'https://keycloak.demo23.gxfs.fr',
    clientId: 'dapps-login',
});
await keycloakConfig.init({ onLoad: 'check-sso' });
export default keycloakConfig;
