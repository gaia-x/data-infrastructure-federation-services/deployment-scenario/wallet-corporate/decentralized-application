import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { createWeb3Modal,defaultWagmiConfig } from '@web3modal/wagmi';
import { http, createConfig, WagmiProvider,WagmiConfig } from 'wagmi';
import { mainnet, sepolia, hardhat } from 'wagmi/chains';
import { walletConnect, injected, coinbaseWallet } from 'wagmi/connectors';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { createRoot } from 'react-dom/client';
// 0. Setup queryClient
const queryClient = new QueryClient();
const projectId = '013520a00cf03587c28a91ccbbb24262';
const chains=[mainnet, sepolia, hardhat];

// 2. Create wagmiConfig
const metadata = {
    name: 'Web3Modal',
    description: 'Web3Modal Example',
    url: 'https://web3modal.com', // origin must match your domain & subdomain
    icons: ['https://avatars.githubusercontent.com/u/37784886']
};
const wagmiConfig= defaultWagmiConfig({
    projectId,
    chains,metadata
});

// 3. Create modal
const web3Modal = createWeb3Modal({
   chains,
    projectId,
    wagmiConfig
});

createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <WagmiConfig config={wagmiConfig} >
                <App />
        </WagmiConfig>
    </React.StrictMode>
);

reportWebVitals();
