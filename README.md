# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
https://gateway.pinata.cloud/ipfs//bagaaierasords4njcts6vs7qvdjfcvgnume4hqohf65zsfguprqphs3icwea
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)

# Walt ID - Déploiement avec Docker Compose

Walt ID est une application d'identification décentralisée (DID) qui peut être déployée facilement à l'aide de Docker Compose. Ce guide vous montrera comment télécharger et déployer Walt ID sur votre propre machine à l'aide de Docker.

## Prérequis
Avant de commencer, assurez-vous d'avoir installé les outils suivants sur votre système :

### Docker : Instructions d'installation Docker
### Docker Compose : Instructions d'installation Docker Compose

## Téléchargement de Walt ID

1. Clonez le dépôt Walt ID depuis GitHub :

`git clone https://github.com/walt-id/waltid-identity.git`

`cd docker-compose`

## Déploiement avec Docker Compose

Utilisez Docker Compose pour construire et démarrer les services Walt ID :

`docker-compose up -d`

## Création d'un Compte Walt ID

### Accès à l'Interface Utilisateur :

Ouvrez votre navigateur web et accédez à l'URL où Walt ID est déployé (par exemple, http://localhost:7101.

### Création d'un Nouveau Compte :

Sur la page d'accueil de Walt ID, recherchez un lien ou un bouton pour créer un nouveau compte. Cela pourrait être étiqueté comme "S'inscrire", "Créer un compte", ou similaire.

## Ajout de Verifiable Credentials (VC)

Une fois que vous êtes connecté à votre compte Walt ID, vous pouvez commencer à ajouter des VCs.

# Lancer le Projet Backend Node.js avec IPFS

Pour lancer un projet backend Node.js qui interagit avec IPFS (InterPlanetary File System), vous aurez besoin de configurer un environnement Node.js, installer les dépendances nécessaires. 
## Étapes pour Cloner, Installer et Lancer le Projet depuis GitLab

1. Cloner le Projet depuis GitLab
   Tout d'abord, assurez-vous d'avoir Git installé sur votre machine. Ensuite, ouvrez un terminal (ou une invite de commande) et exécutez la commande suivante pour cloner le projet depuis GitLab :
   
`git clone https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/wallet-corporate/backendipfs.git`

` cd backendipfs`

2. Installer les Dépendances
   Après avoir cloné le projet, accédez au répertoire du projet et installez les dépendances Node.js à l'aide de npm :

`cd backendipfs`

`npm install`

3. Lancement du Projet

`node index.js`
